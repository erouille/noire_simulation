# REFERENCES

- Rouillé, E., Cecconi, B., Segret, B., & Girard, J. N. (**2023a**). *Radio-Source Tracker: 
  Autonomous Attitude Determination on a Radio Interferometric Swarm*. Submitted to Radio Sci. 
- Rouillé, E., Cecconi, B., Segret, B., & Girard, J. N. (**2023b**). *Radio-Source Tracker: 
  Autonomous Attitude Determination on a Radio Interferometric Swarm: supplementary dataset* 
  (version 1.0) [Data set]. Zenodo. https://doi.org/10.5281/zenodo.7603149
