#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 31 09:54:34 2021

@author: Erwan Rouillé
    erwan.rouille@obspm.fr
"""

import numpy as np
import matplotlib.pyplot as plt
import healpy as hp
import time

import astropy.units as u
from astropy import constants as const
from nenupy.astro.skymodel import HpxGSM

from scipy.spatial.transform import Rotation as R
from scipy import optimize
from scipy.special import sph_harm, spherical_jn
import pyshtools as pysh 


import dask.array as da
from dask.diagnostics import ProgressBar as DaskProgressBar
from dask.diagnostics import Profiler, ResourceProfiler, CacheProfiler
from dask.diagnostics import visualize



#from nenupy.skymodel.pointsources import get_point_sources
from os.path import dirname, abspath, join
from astropy.table import Table, QTable
import astropy.units as u
from astropy.coordinates import SkyCoord
import urllib

def extrapol_flux(freq, rflux, rfreq, index):
    """ Given the ``rflux`` in Jy at the ``rfreq`` in MHz,
        and the spectral index ``index``, extrapolate the ``flux``
        at ``freq`` MHz.
        :param freq:
            Frequency at which the flux extrapolation should be
            done. If `float`, assumed to be given in MHz. 
        :type freq: `float` or :class:`~astropy.units.Quantity`
        :param rflux:
            Reference flux of the source. If `float` assumed to
            be in Jy.
        :type rflux: `float` or :class:`~astropy.units.Quantity`
        :param rfreq:
            Reference frequency at which ``rflux`` was measured.
            If `float`, assumed to be given in MHz.
        :type rfreq: `float` or :class:`~astropy.units.Quantity`
        :param index:
            Spectral index of the power-law source model.
        :type index: `float`
        :returns: Extrapolated flux in Jy.
        :rtype: :class:`~astropy.units.Quantity`
    """
    if not isinstance(freq, u.Quantity):
        freq *= u.MHz
    else:
        freq = freq.to(u.MHz)
    if not isinstance(rflux, u.Quantity):
        rflux *= u.Jy
    else:
        rflux = rflux.to(u.Jy)
    if not isinstance(rfreq, u.Quantity):
        rfreq *= u.MHz
    else:
        rfreq = rfreq.to(u.MHz)

    return (rflux * (freq/rfreq)**index)



def get_point_sources(freq, center, radius):
    skymodel_file = join(
        dirname(abspath(__file__)),
        'Models/lfsky.fits'
    )
    """ Retrieve point sources coordinates and flux from the
        model gathered from the `LOFAR <https://lcs165.lofar.eu/>`_
        database. This returns sources spread over the ``center``
        sky coordinates at a given ``radius`` as well as their
        extrapolated fluxes at the desired frequency ``freq``.
        :param freq:
            Frequency at which the flux extrapolation should be
            done. If `float`, assumed to be given in MHz. 
        :type freq: `float` or :class:`~astropy.units.Quantity`
        :param center:
            Sky coordinates of the center fo the field to look
            for point-sources. 
        :type center: :class:`~astropy.coordinates.SkyCoord`
        :param radius:
            Radius from the ``center`` to search for sources. If
            `float`, assumed to be given in degrees.
        :type radius: `float` or :class:`~astropy.units.Quantity`
        :returns:
            Point source coordinates and their associated fluxes
            (:class:`~astropy.coordinates.SkyCoord`, `~numpy.ndarray`)
        :rtype: `tuple`
    """
    if not isinstance(center, SkyCoord):
        raise TypeError(
            'center must be a SkyCoord object.'
        )
    if not isinstance(radius, u.Quantity):
        radius *= u.deg
    else:
        radius = radius.to(u.deg)
    model_table = Table.read(skymodel_file)
    pointsrc = SkyCoord(
        ra=model_table['ra']*u.deg,
        dec=model_table['dec']*u.deg,
    )
    separations = center.separation(pointsrc)
    srcmask = separations <= radius
    fluxes = extrapol_flux(
        freq=freq,
        rflux=model_table[srcmask]['flux']*u.Jy,
        rfreq=model_table[srcmask]['rfreq']*u.Hz,
        index=model_table[srcmask]['index']
    )
    return pointsrc[srcmask], fluxes

















def res2nside(res,degree=False,method='up_bound'):
    """
    Return the number of nside to use for a Healpix map
    given a resolution (ie pixel size)
    by default the nside is chosen so that the pixel of the map are smaller than the resolution
    """
    if isinstance(res, u.Quantity):
        res = res.to_value(u.rad)
    else:
        if degree:
            res = res*np.pi/180
    nest = np.sqrt(4*np.pi/(12*res**2))
    if method=='up_bound':
        nexp = np.ceil(np.log(nest)/np.log(2))
    elif method=='round':
        nexp = np.round(np.log(nest)/np.log(2))
    elif method=='down_bound':
        nexp = np.floor(np.log(nest)/np.log(2))
    else:
        raise TypeError('wrong input method : up_bound or round')
    nside = int(2**nexp)
    return nside 


def flip_hpmap(mapp):
    """
    Return a flipped version of a healpix map.
    The map is flipped along the z-axis
    (theta --> pi-theta)
    """
    npix = len(mapp)
    nside = hp.npix2nside(npix)
    theta,phi = hp.pix2ang(nside, np.arange(npix))
    id_flip = hp.ang2pix(nside, np.pi-theta, phi)
    mapp_flipped = mapp[id_flip]
    return mapp_flipped




def get_continuum(ns=200, freq=None, resol=None):
    """
    Return the gsm healpix map at a given frequency and resolution
    The brightest point sources are removed from the map
    (please refere to documentation for further information)
    """
    if resol is None:
        resol = 1.83/16*u.deg
    if freq is None:
        freq = 50.0  *u.MHz

    Source_p = Sky()
    Source_p.setup_source_p(ns,freq)
    source_pos = Source_p.source_pos
    
    # old version
    #gsm = HpxGSM(freq=freq, resolution=resol)
    #skymap = gsm.skymap
    gsm = HpxGSM(frequency=freq, resolution=resol)
    skymap =  gsm._generate_gsm_map().compute()[0,0,0,:]
    
    npix = skymap.shape[0]
    nside = hp.npix2nside(npix)
    
    r1 = 1.5/180*np.pi
    r2 = r1*1.25
    
    mapp_clean = np.copy(skymap)
    for i_s in range(ns):
        disc1 = hp.query_disc(nside,source_pos[i_s,:],radius=r1)
        disc2 = hp.query_disc(nside,source_pos[i_s,:],radius=r2)
        disc3 = disc2[np.isin(disc2,disc1,invert=True)]
        val = np.quantile(skymap[disc3],0.2)
        mapp_clean[disc2] = val
        
    mapp_amp_max = np.max(skymap)
    
    return mapp_clean, mapp_amp_max



def brightness_lf(B, f_out, f_in=50*u.MHz):
    """
    Return the brightness of a source at a given frequency
    according to our model (refere to the documentation for further details)
    the input brightness is supposed to be given at a frequency over 2MHz
    and the output is supposed to be lower than 2MHz
    """
    f_turnover = 2*u.MHz

    if not isinstance(B, u.Quantity):
        B_in = B* u.Jy
    else:
        B_in = B
    
    if isinstance(f_out, float):
        f_out = f_out*u.kHz
    elif isinstance(f_out, u.Quantity):
        pass
    else:
        raise TypeError('wrong input f_out: float or u.Quantity')

    if f_out.size==1:
        f_out = np.array([f_out.to_value(u.Hz)])*u.Hz

    if B.size==1:
        B = np.array([B.to_value(u.Jy)])*u.Jy

    T_in = (const.h*f_in/const.k_B/np.log(1+2*const.h*f_in**3/(const.c**2*B_in))).to(u.K)
    T_out = (T_in[None,:] * (f_in/f_turnover)**(2.53) * (f_out[:,None]/f_turnover)**(-0.3) ).to(u.K)
    B_out = (2*const.h*f_out[:,None]**3/const.c**2 /(np.exp((const.h*f_out[:,None])/(const.k_B*T_out))-1)).to(u.Jy)


    if B.size==1 and f_out.size==1:
        T_out = T_out[0,0]
        B_out = B_out[0,0]
    elif B.size==1 and f_out>1:
        T_out = T_out[:,0]
        B_out = B_out[:,0]
    elif f_out.size==1 and B.size>1:
        T_out = T_out[0,:]
        B_out = B_out[0,:]

    return B_out, T_out


def source_extended(pos, res, flux, radius=None, rotation=None):
    """
    Return a 'Sky' object with sources representing a uniform disc
    of radius 'radius' in radiant 
    of total brightness equal to the 'flux' input
    the distance between the sources is set to match the input resolution 'res'

    The center position is set with the input 'pos' and a rotation can be applied to it using the input 'rotation'
    """

    if isinstance(pos, Position):
        pos = pos.vec
    elif isinstance(pos, np.ndarray):
        pass
    elif isinstance(pos, list):
        pos = np.array(pos)
    else:
        raise TypeError('wrong input type for pos')

    if isinstance(res, u.Quantity):
        res = res.to_value(u.rad)
    elif isinstance(res, float):
        pass  #assume in rad
    else:
        raise TypeError('wrong input type for res')

    if radius is None:
        # assume Sun
        sun_diameter = 696340*2*u.km # rf emission radius ????
        sun_earth_dist = 150e6*u.km 
        radius = sun_diameter/sun_earth_dist * u.rad
    elif isinstance(radius, float):
        radius = radius *u.rad
    elif isinstance(radius, u.Quantity):
        pass
    else:
        raise TypeError('wrong input type for radius')

    if rotation is None:
        pass
    elif isinstance(rotation, R):
        pos = rotation.apply(pos)
    elif isinstance(rotation, np.ndarray):
        if rotation.shape == (3,3):
            rotation = R.from_matrix(rotation)
        else:
            raise TypeError('rotation must be 3x3 or scipy.spatial.transform.Rotation')
    else:
        raise TypeError('wrong input type for rotation')

    gain = 10

    nside = 2**int(np.ceil(np.log(4*np.pi/12/(res/gain))/np.log(2)))
    indices = hp.query_disc(nside, pos, radius=radius.to_value(u.rad))

    flux_pix = flux/len(indices)
    Sun_sky = Sky()
    for i,e in enumerate(indices):
        source = Source(Coord=Position(pos=np.array(hp.pix2vec(nside,e))), flux=flux_pix)
        Sun_sky.add_source(Source=source)

    return Sun_sky





def compare_rotation_old(Rot1, Rot2, nvec=1000):
    """
    This function is meant to compare the simulated rotation and the measured rotation
    It uses a Monte-Carlo method with a set of 'nvec' random vectors
    It returns the list of the errors made for every vectors and its mean value
    """
    
    ph = (np.random.rand(nvec)-0.5)*2*np.pi
    ss = (np.random.rand(nvec)-0.5)*2
    cc = np.sqrt(1-ss**2)
    
    xx = cc*np.sin(ph)
    yy = cc*np.cos(ph)
    zz = ss
    
    vec_list = np.empty((nvec,3))
    vec_list[:,0] = xx
    vec_list[:,1] = yy
    vec_list[:,2] = zz

    
    err = np.zeros(nvec)

    for i in range(nvec):
        err[i] = np.arccos(  np.dot(Rot1.apply(vec_list[i,:]), Rot2.apply(vec_list[i,:]))  )
        
    avg = np.mean(  err[ np.where(np.invert(np.isnan(err))) ]    )
    return err, avg



def compare_rotation(Rot1, Rot2):
    """
    This function is meant to compare the simulated rotation and the measured rotation
    It computes the maximum angular error that can be observed for any vector
    """
    Rot3 = (Rot1.inv())*Rot2
    n = Rot3.as_quat()[:3]
    n = n/np.linalg.norm(n)
    ex = np.array([1,0,0])
    vpx = np.cross(n, ex)
    vpx = vpx / np.linalg.norm(vpx)

    err = np.arccos(np.sum(Rot1.apply(vpx)*Rot2.apply(vpx),axis=-1))
    return err*u.rad


        


def gaussian_healpix2(amp,theta_c,phi_c,sig_rad,bkg,nside):
    P1 = Position(ang=np.array([theta_c,phi_c])*u.rad)
    return lambda ipix:  bkg + np.abs(amp)*np.exp(-( ( P1.angle_vec(Position(ang=np.array(hp.pix2ang(nside,ipix))*u.rad))/sig_rad)**2  ).value/2)    
  
    
def gaussian2d(amp,x0,y0,sig,bkg):
    return lambda x,y : bkg + amp*np.exp(-( (x-x0)**2/sig**2 + (y-y0)**2/sig**2  ))
    
  




def pixlist2radecmap(pixlist,indices,nside,vec,fov,pos_fit=None):
    """
    Display function
    
    Return 2d-array representing a region of healpix map
    """
    res = hp.nside2resol(nside)
    
    r_grid_ini = np.array(hp.pix2vec(nside,indices)).T
    theta0, phi0 = hp.vec2ang(vec)
    rot = R.from_euler('zy',np.array([-phi0,np.pi/2-theta0]).T[0])
    r_grid = rot.apply(r_grid_ini)
    theta_rot, phi_rot = hp.vec2ang(r_grid)
    
    npix = int(2*np.ceil(fov/res))+1
    mapp = np.zeros((npix,npix))*np.nan
    theta = np.linspace(-fov,fov,npix)+np.pi/2
    phi = np.linspace(-fov,fov,npix)
    phis, thetas = np.meshgrid(phi, theta)
    theta_arr = np.ravel(thetas)
    phi_arr = np.ravel(phis)
    unrav_idx = np.unravel_index(np.arange(npix**2),(npix,npix))
             
    pix_vec = hp.ang2vec(theta_arr,phi_arr)
    pix_vec_rot = rot.inv().apply(pix_vec)
    idx = hp.vec2pix(nside,pix_vec_rot[:,0],pix_vec_rot[:,1],pix_vec_rot[:,2])
    pix_idx = np.argmin(np.abs(indices[None,:]-idx[:,None]),axis=-1)
    mapp_rav = pixlist[pix_idx]
    mapp_rav[np.where(np.min(np.abs(indices[None,:]-idx[:,None]),axis=-1)>1)] = np.nan
    mapp[unrav_idx] = mapp_rav
    
    if pos_fit is not None:
        pos_vec = pos_fit.vec
        pos_rot = rot.apply(pos_vec)
        pos_ang = np.array(hp.vec2ang(pos_rot)).T[0]
        pos_pixx = (pos_ang[0]-np.pi/2)/res + npix/2
        pos_pixy = (np.mod(pos_ang[1]+np.pi,2*np.pi)-np.pi)/res + npix/2
        pos = np.array([pos_pixx,pos_pixy])
    else:
        pos=None
    
    return mapp, pos






def sph_harm_sh(lmax,theta,phi):
    """
    Return the values of the spherical harmonics as an array

    Warning : the harmonic convention implemented might not be the suited for SWHT
    futher tests have to be run
    """

    lmax = int(lmax)
    l,m = hp.Alm.getlm(lmax)

    if isinstance(theta, np.ndarray):
        if theta.shape!=phi.shape:
            raise TypeError("the shapes do not match (theta,phi)")

        if len(theta.shape)==1:
            pol = np.array(list(map(lambda x: pysh.legendre.PlmON(lmax,np.cos(x)), theta)))
            pol_sort = pol[:,pysh.legendre.PlmIndex(l,m)]
            harms_map = pol_sort*np.exp(1j*m[None,:]*phi[:,None])           *np.sqrt(2-(m[None,:]==0).astype(int))*(-1)**m[None,:]
            harms_map = harms_map.T

            #npos = len(theta)
            #harms_map = np.zeros((len(l),npos),dtype=np.complex128)
            #for i in range(npos):
            #    pol = pysh.legendre.PlmON(lmax,np.cos(theta[i]))
            #    pol_sort = pol[pysh.legendre.PlmIndex(l,m)]
            #    harms = pol_sort*np.exp(1j*m*phi[i])           *np.sqrt(2-(m==0).astype(int))*(-1)**m
            #    harms_map[:,i] = harms

        elif len(theta.shape)==2:
            n1,n2 = theta.shape
            harms_map = np.zeros((len(l),n1,n2),dtype=np.complex128)
            for i in range(n1):
                for j in range(n2):
                    pol = pysh.legendre.PlmON(lmax,np.cos(theta[i,j]))
                    pol_sort = pol[pysh.legendre.PlmIndex(l,m)]
                    harms = pol_sort*np.exp(1j*m*phi[i,j])     *np.sqrt(2-(m==0).astype(int))*(-1)**m
                    harms_map[:,i,j] = harms
        else:
            raise TypeError("theta and phi has too much dimension: max=2")

    else:
        pol = pysh.legendre.PlmON(lmax,np.cos(theta))
        pol_sort = pol[pysh.legendre.PlmIndex(l,m)]
        harms = pol_sort*np.exp(1j*m*phi)                      *np.sqrt(2-(m==0).astype(int))*(-1)**m
        harms_map = harms

    return harms_map.T






def blm_all(lmax,vis,b_sph,k):
    """
    Compute the blm coefficients of the spherical harmonic decomposition of the brightness
    """
    if isinstance(k, u.Quantity):
        k = k.to_value(1/u.m)
    if isinstance(vis, u.Quantity):
        vis = vis.to_value(u.Jy)

    l,m = hp.Alm.getlm(lmax)
    harms = sph_harm_sh(lmax,da.from_array(b_sph[:,1],chunks=(100)),da_from_array(b_sph[:,2],chunks=(100)))
    bessel = spherical_jn(l[None,:],k*da.from_array(b_sph[:,0,None],chunks=(100)))
    vlm = 2*k**2/np.pi * np.sum( vis[:,None] * bessel * harms ,axis=0)
    blm_da = vlm/(4*np.pi*(-1j)**l)
    with DaskProgressBar():
        blm = blm_da.compute()
    return blm



    
    
    
    
def grid3d(bl,vis,nside=None,resol=None,n_mesh=None):
    
    gain = 1.01
    
    if nside is not None:
        dl = np.sqrt(hp.nside2pixarea(nside)/3)
    elif resol is not None:
        dl = np.tan(resol.to_value(u.rad)/8*np.sqrt(3))
    else:
        dl = None
    
    if n_mesh is None:
        if dl is not None:
            n_mesh = int(np.ceil(1/dl*gain))*2+1
        else:
            n_mesh=301
            
    if dl is not None:
        du = 1/(n_mesh*dl)
    else:
        du=1/2
    
    
    vis_grid = np.zeros((n_mesh,n_mesh,n_mesh),dtype=np.complex128)
    for i in range(bl.shape[0]):
        iu = int(n_mesh/2 + bl[i,0]/du)
        iv = int(n_mesh/2 + bl[i,1]/du)
        iw = int(n_mesh/2 + bl[i,2]/du)
        if (np.abs(iu-n_mesh/2)<n_mesh/2) and (np.abs(iv-n_mesh/2)<n_mesh/2) and (np.abs(iw-n_mesh/2)<n_mesh/2): 
            vis_grid[iu,iv,iw] = vis[i]
        
    return vis_grid, dl


def make_mapp_fft3d_bis(vis_grid, nside, dl):
    n_mesh = vis_grid.shape[0]
    img_cube = np.fft.fftshift(   np.fft.fftn(  np.fft.ifftshift(vis_grid)))
    img_cube = np.flip(img_cube,axis=0)
    img_cube = np.flip(img_cube,axis=1)
    img_cube = np.flip(img_cube,axis=2)
    
    npix = hp.nside2npix(nside)
    mapp = np.zeros(npix,dtype=np.complex128)
    
    indices = np.arange(npix)
    r_grid = np.array(hp.pix2vec(nside,indices))
    pos = np.round(r_grid/dl + n_mesh/2).astype(int)
    pos_idx = tuple(pos)
    
    mapp[indices] = img_cube[pos_idx]
    
    return mapp
    



    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
class Position(object):
    """
    Object reprensenting the position of an object
    can either be the position of a satellite of the swarm or a direction in the sky
    mostly UNUSED
    could be replaced by astropy.SkyCoord
    """
    
    def __init__(self,pos=None,ang=None,r=None,unit=None,ang_unit=None):
        
        if pos is not None:
            if unit is not None:
                pos = pos*unit
            else:
                if not isinstance(pos, u.Quantity):
                    pos *= u.m
            
            self.pos = pos
            self.unit = pos.unit
            self.get_ang()
                
        elif r is not None:
            if not isinstance(r, u.Qantity):    
                if unit is not None:
                    r = r*unit
                else :
                    r = r*u.m  
            self.r = r
            self.unit = r.unit
        elif r is None:
            if unit is not None:
                self.unit = unit
            else:
                self.unit = u.m
            self.r = 1*self.unit       
        
        if ang is not None:
            if (ang_unit is not None) and (not isinstance(ang, u.Quantity)):
                ang *= ang_unit
            else:
                if not isinstance(ang, u.Quantity):
                    ang *= u.rad
            self.ang = ang
            self.unit = u.m
            self.get_vec()
            

    @property    
    def vec(self):
        return self.pos.to_value(u.m)
    
    @property    
    def norm(self):
        norm = np.sqrt(np.sum(self.vec**2)) *u.m
        self._norm = norm
        return norm
    
    def dist_to(self,Position2):
        dist = np.sqrt(np.sum((self.vec-Position2.vec)**2)) *u.m
        return dist
    
    def apply_rotation(self,Rot):
        vec_rot = Rot.apply(self.vec)
        vec_rot *= self.unit
        self.pos = vec_rot
        self.get_ang()
        
    def get_rotated(self,Rot):
        xyz = Rot.apply(self.vec)
        xyz *= self.unit
        return xyz
    
    def get_ang(self):
        vec = self.vec
        r = np.sqrt(np.sum(vec**2))
        phi = np.arctan2(vec[1],vec[0])
        theta = np.arccos(vec[2]/r)
        if np.isnan(theta):
            theta = 0
        r *= self.unit
        ang = np.array([theta,phi])*u.rad
        self.ang = ang
        self.r = r
        return ang, r
    
    def get_vec(self):
        ang = self.ang
        r = self.r
        x = r*np.sin(ang[0])*np.cos(ang[1])
        y = r*np.sin(ang[0])*np.sin(ang[1])
        z = r*np.cos(ang[0])
        vec = np.array([x.value,y.value,z.value])
        pos = vec*self.unit
        self.pos = pos
        return pos
    
    def dot(self,Position2):
        vec1 = self.vec
        vec2 = Position2.vec
        prod = np.dot(vec1,vec2)
        return prod
    

    def angle_vec(self,Pos2):
        """
        need rework
        """
        angle = np.arccos(self.dot(Pos2))
        if isinstance(angle, np.ndarray):
            angle[np.where(np.isnan(angle))] = 0
        else:
            if angle==np.nan:
                angle = 0
        return angle*u.rad
    
    
    
  
    
class Satellite(object):
    """
    Object reprensenting a satellite of the swarm
    mostly defined by its position (for now)
    but can be upgraded to include antenna orientation/pattern, velocity, calibration parameters,...
    """
    
    def __init__(self,pos,ant_ori=None):
        if isinstance(pos, Position):
            self.Pos = pos
        else:
            self.Pos = Position(pos=pos)
            
        if (ant_ori is None) or (ant_ori==False):
            self.has_ant = False
        elif ant_ori == True:
            self.has_ant = True
            self.ant_ori = np.array([[1,0,0],[0,1,0],[0,0,1]])
        else:
            self.ant_ori = ant_ori
            self.has_ant = True


    def set_pos(self,pos):
        if isinstance(pos, Position):
            self.Pos = pos
        else:
            self.Pos = Position(pos=pos)
            
    def set_ant_ori(self,ant_ori):
        self.has_ant = True
        self.ant_ori = ant_ori
        

        
        
class Swarm(object):
    
    def __init__(self,satlist=None, sat_pos=None):
        if satlist is not None:
            self.satlist = satlist
        else:
            if sat_pos is not None:
                self.update_satlist(sat_pos)
            else:
                self.satlist = []
        self._satpos = self.satpos 
        self._unit = self.unit
        self._n_sat = self.n_sat
            
    @property
    def n_sat(self):
        return len(self.satlist)
    
    @property
    def satpos(self):
        satpos = np.zeros((self.n_sat,3))
        for i in range(self.n_sat):
            satpos[i,:] = self.satlist[i].Pos.vec
        self._satpos = satpos
        return satpos

    @property
    def d_max(self):
        b = self.baseline(mode='tril')
        d_max = np.max(np.linalg.norm(b,axis=-1))
        return d_max
    
    @property
    def ant_ori(self):
        ant_ori = np.zeros((self.n_sat,3))
        for i in range(self.n_sat):
            ant_ori[i,:] = self.sat_list[i].ant_ori
    
    @property    
    def unit(self):
        if len(self.satlist)==0:
            self._unit = u.m
        else:
            self._unit = self.satlist[0].Pos.unit
        return self._unit
    
    
    def update_individual_sat(self):
        n_sat = self._n_sat
        for i in range(n_sat):
            self.satlist[i].set_pos(self._satpos[i,:]*self._unit)
            
    def update_satlist(self,satpos):
        n_sat = satpos.shape[0]
        self.satlist = []
        for i in range(n_sat):
            self.add_sat(Satellite(pos=Position(pos=satpos[i,:])))
        self._satpos = satpos 
        self._n_sat = n_sat
        
                        
    def add_sat(self,Satellite):
        self.satlist.append(Satellite)
        self._satpos = self.satpos 
        self._unit = self.unit
        self._n_sat = self.n_sat
        
    def __getitem__(self,n):
        return self.satlist[n]
            
        
    def make_random(self,n_sat,d_max,distrib='uni',ant_ori=False):
        if not isinstance(d_max, u.Quantity):
            d_max *= u.m
        for i in range(n_sat):
            if distrib == "uni":
                #pos = np.random.rand(3)*d_max
                s = (np.random.rand()-0.5)*2
                ph = (np.random.rand()-0.5)*2*np.pi
                uu = np.random.rand()
                r = (d_max/2).to_value(u.m)*(uu)**(1/3)
                cc = r*np.sqrt(1-s**2)
                pos = np.array([cc*np.cos(ph), cc*np.sin(ph), r*s])
                
            elif distrib=='gauss':
                pos = np.random.randn(3)*d_max

            else:
                raise TypeError('wrong distrib')
                
            if isinstance(ant_ori, bool):
                ori = ant_ori
            elif ant_ori == 'rand':
                pass#TODO
            else:
                ori = ant_ori[i]
            
            Sat = Satellite(pos, ant_ori=ori)
            self.add_sat(Sat)    
        self._n_sat = n_sat
        self._satpos = self.satpos 
        self._unit = self.unit
               
            
    def baseline(self,mode='all'):

        if mode == 'all':
            row = np.arange(self.n_sat)
            col = np.arange(self.n_sat)
            roww,coll = tuple(np.meshgrid(row,col))
            roww = roww.flatten()
            coll = coll.flatten()
            indices = (coll,roww)       
        elif mode == 'tril':
            indices = np.tril_indices(self.n_sat)
        elif mode == 'tril_nodiag':
            indices = np.tril_indices(self.n_sat,k=-1)
        elif mode == 'solo_diag':
            indices = (np.arange(self.n_sat),np.arange(self.n_sat))
        else:
            raise TypeError('wrong input mode :  (all, tril, tril_nodiag, solo_diag)')

        satpos = self._satpos
        b = satpos[indices[0],:]-satpos[indices[1],:]
        b *= self._unit

        return b

    
    def plot(self):
        fig,ax = plt.subplots(1,1)
        ax = fig.add_subplot(111, projection='3d')
        #ax.set_aspect('equal')
        ax.set_aspect('auto')
        ax.set_xlabel('x (km)')
        ax.set_ylabel('y (km)')
        ax.set_zlabel('z (km)')
        ax.set_title('Satellite positions')
        
        satpos = self.satpos /1e3
        ax.scatter(satpos[:,0],satpos[:,1],satpos[:,2], marker=".")
        



 
class Source(object):
    
    def __init__(self,Coord=None, flux=None, flux_unit=u.Jy):
    
        if Coord is None:
            self.Coord=Position(pos=np.array([1,0,0]))
        elif isinstance(Coord, Position):
            self.Coord = Coord
        else:
            self.set_pos(vec=Coord)
        
        if flux is None:
            self.flux = 0*u.Jy
        elif isinstance(flux,u.Quantity):
            self.flux = flux
        else:
            self.flux = flux *u.Jy 


    @property
    def vec(self):
        return self.Coord.vec

    @property
    def ang(self):
        return self.Coord.get_ang[0]
            
        
    def set_pos(self,ang=None,vec=None,Pos=None):
        if Pos is not None:
            self.Coord = Pos
        elif ang is not None:
            self.Coord = Position(ang=ang)
        elif vec is not None:
            self.Coord = Position(pos=vec)
        else:
            raise TypeError('no input')

    #def set_rand_pos(self):        
            
    def set_flux(self,flux,flux_unit=u.Jy):
        if not isinstance(flux, u.Quantity):
            flux *= flux_unit
        self.flux = flux
        

    def angle_between(self,Source2):
        return self.Coord.angle_vec(Source2.Coord)
    
    def angle_3source(self,Source2,Source3):
        a = self.angle_between(Source2)
        b = self.angle_between(Source3)
        c = Source2.angle_between(Source3)
        gamma = np.arccos( (np.cos(c)-np.cos(a)*np.cos(b))/(np.sin(a)*np.sin(b))  )
        return gamma#*u.rad

    def copy(self):
        return Source(Coord=self.Coord, flux=self.flux)





         
class Sky(object):


    def __init__(self,source_pos=None,fluxes=None, source_list=None):
        if source_list is not None:
            self.from_source_list(source_list)

        else:
            if source_pos is not None:
                if source_pos.shape[-1]==3:
                    self.source_pos = source_pos
                elif source_pos.shape[-1]==2:
                    self.source_pos = hp.ang2vec(source_pos)
            else:
                self.source_pos = np.array([]).reshape(-1,3)

            if fluxes is not None:
                if isinstance(fluxes,u.Quantity):
                    self.fluxes = fluxes
                else:
                    self.fluxes = fluxes *u.Jy
            else:
                self.fluxes = np.zeros((self.ns))*u.Jy



    def __getitem__(self,i):
        if isinstance(i,slice):
            S_item = Sky(source_pos=self.source_pos[i,:], fluxes=self.fluxes[i])
        else:
            S_item = Source(Coord=self.source_pos[i],flux=self.fluxes[i])
        return S_item

    def __add__(self,Sky2):
        self.source_pos = np.concatenate((self.source_pos, Sky2.source_pos)).reshape(-1,3)
        self.fluxes = np.concatenate((self.fluxes, Sky2.fluxes))

    @property
    def ns(self):
        return self.source_pos.shape[0]


    def sort(self,invert=False):
        id_sort = np.argsort(self.fluxes)
        if not invert:
            id_sort = np.flip(id_sort)
        self.source_pos = self.source_pos[id_sort]
        self.fluxes = self.fluxes[id_sort]


    def from_source_list(self, source_list):
        source_pos = []
        fluxes = []
        for i in range(source_list.shape[0]):
            source_pos.append(source_list[i].Coord.vec)
            fluxes.append(source_list[i].flux.to_value(u.Jy))
        self.source_pos = np.array(source_pos)
        self.fluxes = np.array(fluxes)*u.Jy


    def add_source(self, pos=None, flux=None, Source=None):
        if Source is not None:
            self.source_pos = np.concatenate((self.source_pos, Source.Coord.vec.reshape(-1,3)))
            self.fluxes = np.concatenate((self.fluxes, [Source.flux]))
        else:
            if pos is not None:
                self.source_pos = np.concatenate((self.source_pos, np.array(pos).reshape(-1,3)))
            else:
                self.source_pos = np.concatenate((self.source_pos, np.array([1,0,0]).reshape(-1,3)))

            if flux is not None:
                if isinstance(flux, u.Quantity):
                    self.fluxes = np.concatenate((self.fluxes, [flux]))
                else:
                    self.fluxes = np.concatenate((self.fluxes, [flux*u.Jy]))
            else:
                self.fluxes = np.concatenate((self.fluxes, [0*u.Jy]))


    def apply_rotation(self, Rot):
        self.source_pos = Rot.apply(self.source_pos)

    def apply_flip(self):
        self.source_pos = self.source_pos * np.array([1,1,-1])

    def copy(self):
        copy = Sky(source_pos=np.copy(self.source_pos),fluxes=np.copy(self.fluxes))
        return copy

    def source_ang(self,tuple=False):
        ang = hp.vec2ang(self.source_pos)
        if not tuple:
            ang = np.array([ang[0],ang[1]]).T
        return ang


    def angle_between(self,i,j):
        angle = np.arccos(np.sum(self.source_pos[i,:]*self.source_pos[j,:],axis=-1))*u.rad
        if len(angle.shape)>=1:
            angle[np.where(np.isnan(angle))] = 0*u.rad
        else:
            if np.isnan(angle):
                angle = 0*u.rad
        return angle

    def angle_3source(self,i,j,k):
        a = self.angle_between(i,j)
        b = self.angle_between(i,k)
        c = self.angle_between(j,k)
        gamma = np.arccos( (np.cos(c)-np.cos(a)*np.cos(b))/(np.sin(a)*np.sin(b)) )
        if len(gamma.shape)>=1:
            gamma[np.where(np.isnan(gamma))] = 0*u.rad
        else:
            if np.isnan(gamma):
                gamma = 0*u.rad
        return gamma

        
    def setup_source_p(self,ns,freq):
        sources, fluxes = get_point_sources(
            freq=freq,
            center=SkyCoord(ra=0*u.deg, dec=0*u.deg),
            radius=180*u.deg
        )    
        
        arg = np.argsort(-fluxes)[:ns]
        
        self.fluxes = fluxes[arg]
        
        theta = (np.pi/2-sources.dec.rad)[arg]
        phi = (sources.ra.rad)[arg]
        source_pos = hp.ang2vec(theta, phi)

        self.source_pos = source_pos


    def gather_sources(self,radius):
        """
        This function gathers source that are closer than a given radius as a single source
        """
        if isinstance(radius, u.Quantity):
            pass
        else:
            radius = radius *u.rad
        
        Sky_sort = self.copy()
        Sky_sort.sort()

        n_spot = Sky_sort.ns
        used_indices = []
        Sky_gather = Sky()
  
        for i in range(n_spot):
            if np.sum(np.isin(np.array(used_indices), i)):
                continue
            else:
                used_indices.append(i)
                source_beam_pos = [ Sky_sort[i].vec ]
                source_beam_amp = [ Sky_sort[i].flux.to_value(u.Jy) ]
                for j in range(i+1,n_spot):                   
                    if Sky_sort.angle_between(i,j) < radius:
                        used_indices.append(j)
                        source_beam_pos.append(Sky_sort[j].vec)
                        source_beam_amp.append(Sky_sort[j].flux.to_value(u.Jy))
                        
                #source_avg = np.mean(np.array(source_beam_pos),axis=0)
                source_avg = np.mean(np.array(source_beam_pos)*np.array(source_beam_amp)[:,None],axis=0)
                source_avg = source_avg/np.linalg.norm(source_avg)#,axis=-1)
                if np.isnan(source_avg).any():
                    source_avg = np.mean(np.array(source_beam_pos),axis=0)
                source_amp = np.sum(np.array(source_beam_amp))

                Sf = Source(Coord=Position(pos=source_avg),flux=source_amp)
                Sky_gather.add_source(Source=Sf)

        return Sky_gather


        
    def plot(self,mapp=None):
        if mapp is None:
            mapp_plot = np.zeros(hp.nside2npix(2**4))
        else:
            mapp_plot = mapp
        hp.mollview(mapp_plot)
        hp.projscatter(hp.vec2ang(self.source_pos), marker='x', color='r')        
        








     
        

    



class Interferometer(object):
    """
    Object holding information on the interferometer: its swarm, the simulated sky
    and its informations: visibilities, 
    """
    
    def __init__(self,auto_setup=False):
        if auto_setup:
            self.setup_swarm(50, 100e3)
            self.set_freq(np.array([50,400])*u.kHz)
            self.setup_sky()
            self.init_sky_list()
            self.init_visibilities()
        
    def set_swarm(self,Swarm):
        self.Swarm = Swarm
        if not hasattr(self, 'baseline_mode'):
            self.baseline_mode = 'all'
        self.b = self.Swarm.baseline(mode=self.baseline_mode)

    def set_baseline_mode(self, baseline_mode):
        if np.sum(np.isin(np.array(['all','tril','tril_nodiag','solo_diag']),baseline_mode))>=1:
            self.baseline_mode = baseline_mode
        else:
            raise TypeError('wrong input mode :  (all, tril, tril_nodiag, solo_diag)')

    @property
    def n_sat(self):
        return self.Swarm.n_sat
        
    def set_sky(self,Sky):
        self.Sky = Sky
    
    def set_freq(self,f):
        if isinstance(f,float):
            f = f*u.Hz 
        elif isinstance(f,u.Quantity):
            pass
        else:
            raise TypeError('wrong type input for f')
        self.f = f


    def add_freq(self,f,auto_compute=False):
        if isinstance(f,float):
            f = f*u.Hz 
        elif isinstance(f,u.Quantity):
            pass
        else:
            raise TypeError('wrong type input for f')

        self.f = np.concatenate(self.f, f)
        self.visibilities = np.concatenate((self.visibilities,np.zeros((f.size,self.b.shape[0]),dtype=np.complex128)*u.Jy))
        self.sysT = np.concatenate((self.sysT,np.nan))
        for i in range(f.size):
            self.continuum_list.append(np.nan)
            self.Sky_list.append(np.nan)

        if auto_compute:
            new_f_id = np.arange(len(self.f))[-f.size:]
            self.compute_skies(freq_id=new_f_id)
            self.compute_vis(freq_id=new_f_id)

    
    @property
    def k(self):
        k = 2*np.pi*self.f/const.c
        self._k = k
        return k
    
    @property
    def resol(self):
        if not hasattr(self, 'd_max'):
            self.d_max = self.Swarm.d_max
        resol = const.c/self.f/self.d_max   *u.rad
        self._resol = resol.to(u.rad)
        return self._resol

    def init_visibilities(self):
        self.visibilities = np.zeros((self.f.shape[0],self.b.shape[0]),dtype=np.complex128)*u.Jy
        self.visibilities[:] = np.nan

    def init_sky_list(self):
        self.sysT = np.zeros((self.f.shape[0]))*u.K
        self.sysT[:] = np.nan
        self.continuum_list = [np.nan for i in range(self.f.shape[0])]
        self.Sky_list = [np.nan for i in range(self.f.shape[0])]

#    def set_ant_ori(self,ant_ori=None):
#        if ant_ori is None:
#            ori = self.Swarm.ant_ori
#        elif ant_ori == 'sun':
#            sun_pos = self.Sun.Coord.vec
#            vec1 = np.cross(sun_pos,[1,0,0])
#            vec2 = np.cross(sun_pos,vec1)
#            ori = np.array([sun_pos,vec1,vec2])
#        elif type(ant_ori) == np.ndarray:
#            ori = ant_ori
#        else:
#            raise TypeError('wrong ant_ori input : None, sun, ndarray')
#        self.ant_ori = ori
    
     
        
  
    def setup_swarm(self,n_sat,d_max,distrib='uni',baseline_mode='all',use_ant=False):
        self._n_sat = n_sat
        self.d_max = d_max
        self.set_baseline_mode(baseline_mode)

        Nodes = Swarm()
        Nodes.make_random(n_sat,d_max,distrib=distrib,ant_ori=use_ant)
        self.set_swarm(Nodes)
        #return Nodes
   

    def setup_sky(self, ns=200, skyrot=None, flip=False, continuum_resol_max=None, freq_gsm=50):
        
        if skyrot is None:
            skyrot = R.from_euler('zyx',[0,0,0])
            self.skyrot = skyrot
            mrot_hp = hp.rotator.Rotator(rot=(0,0,0))
        elif skyrot == 'rand':
            yaw = np.random.rand()*np.pi*2
            pitch = np.random.rand()*np.pi#*2
            roll = np.random.rand()*np.pi*2
            skyrot = R.from_euler('zyx',[yaw,pitch,roll])
            self.skyrot = skyrot
            rot_hp = (-yaw*180/np.pi,pitch*180/np.pi,-roll*180/np.pi)
            mrot_hp = hp.rotator.Rotator(rot=rot_hp)
        else:
            self.skyrot = skyrot

        if flip==False:
            self.flip_bool=False
        elif flip==True:
            self.flip_bool=True 
        elif flip=='rand':
            self.flip_bool = (np.random.rand()>0.5)
        else:
            self.flip_bool=False
            raise TypeError('wrong input flip : False, True, rand  \n default False applied')

        
        self.freq_gsm = freq_gsm
        Source_p = Sky()
        Source_p.setup_source_p(ns,self.freq_gsm)
        
        amp_max = np.max(Source_p.fluxes.to_value(u.Jy))
        #amp_max = np.max(Source_p.fluxes)
        
        #original sky
        self.Sky_ori = Source_p.copy()

        
        if self.flip_bool:
            Source_p.apply_flip()
        Source_p.apply_rotation(skyrot)
        
        self.Source_p = Source_p


        if continuum_resol_max is None:
            continuum_resol_max = np.min(self.resol)/4
        elif isinstance(continuum_resol_max, u.Quantity):
            continuum_resol_max = continuum_resol_max
        elif isinstance(continuum_resol_max, float):
            continuum_resol_max = continuum_resol_max*u.rad
        else:
            raise TypeError('wrong input format continuum_resol_max: None, float, u.Quantity')
    
        continuum, continuum_amp_max = get_continuum(ns=200,resol=continuum_resol_max)
        continuum_noclean, _ = get_continuum(ns=0,resol=continuum_resol_max)

        if self.flip_bool:
            continuum = flip_hpmap(continuum)
        continuum = mrot_hp.rotate_map_pixel(continuum)

        self.factor = amp_max/continuum_amp_max
        self.continuum_ref = continuum*self.factor
        self.continuum_noclean = continuum_noclean*self.factor 

        
        


    def compute_skies(self,freq_id=None):
        if freq_id is None:
            freq_id = np.arange(self.f.size)
        if freq_id.size==1:
            freq_id = [freq_id]
        for i,f_id in enumerate(freq_id):
            resol_min = np.sqrt(hp.nside2pixarea(hp.npix2nside(len(self.continuum_ref))))*u.rad
            if  resol_min > self.resol[f_id]:
                print('WARNING')
            nside_rec = res2nside(self.resol[f_id])
            if nside_rec<hp.npix2nside(len(self.continuum_ref)):
                continuum  = hp.ud_grade(self.continuum_ref, nside_rec)
            else:
                continuum = np.copy(self.continuum_ref)

            continuum *= hp.nside2pixarea(hp.npix2nside(len(continuum)))
            continuum_shift, continuum_shift_T = brightness_lf(continuum, self.f[f_id], f_in=self.freq_gsm*u.MHz)
            fluxes_shift, source_sp_T = brightness_lf(self.Source_p.fluxes, self.f[f_id], f_in=self.freq_gsm*u.MHz)
            continuum_shift /= hp.nside2pixarea(hp.npix2nside(len(continuum)))

            self.sysT[f_id] = np.mean(continuum_shift_T,axis=-1) + (np.sum(source_sp_T,axis=-1))/(4*np.pi)

            self.continuum_list[f_id] = continuum_shift
            self.Sky_list[f_id] = Sky(source_pos=np.copy(self.Source_p.source_pos), fluxes=np.copy(fluxes_shift))




    def compute_vis(self,freq_id=None, mode_sp=True, mode_map=True, mode_signal=False, add_to_visibilities=True):

        if self.f.size>=2:
            if freq_id is None:
                freq_id = np.arange(self.f.size) 
            elif isinstance(freq_id, int) or isinstance(freq_id, np.int64) or isinstance(freq_id, np.int32):
                if ( (freq_id>=0) and (freq_id<self.f.size) ):
                    freq_id = freq_id
                else:
                    raise TypeError('freq_id value out of scope')
            elif isinstance(freq_id, np.ndarray) or isinstance(freq_id, list):
                if isinstance(freq_id[0], int) or isinstance(freq_id[0], np.int64) or isinstance(freq_id[0], np.int32):
                    if ( (np.min(freq_id)>=0) and (np.max(freq_id)<self.f.size) ):
                        freq_id = np.array(freq_id)
                    else:
                        raise TypeError('wrong freq index (input freq was not an integer)')
                else:
                    raise TypeError('wrong input type for freq')
        elif self.f.size==1:
            freq_id = 0
        else:
            raise TypeError('Interferometer has no frequency')

        if isinstance(freq_id, int) or isinstance(freq_id, np.int64) or isinstance(freq_id, np.int32):
            freq_id = np.array([freq_id])

        f = self.f[freq_id]
        if f.size==1:
            f = np.array([f.to_value(u.Hz)]) *u.Hz

        print('frequencies',f, freq_id)


        dim0 = self.f.size
        if mode_signal:
            unit = u.Jy**(1/2)
            dim1 = self.n_sat
        else:
            unit = u.Jy
            dim1 = self.b.shape[0]

        if mode_sp:
            vis_sp = np.zeros((dim0,dim1),dtype=np.complex128)*unit
        if mode_map:
            vis_map = np.zeros((dim0,dim1),dtype=np.complex128)*unit


        for i,ff in enumerate(f):
            if mode_sp:
                print("START compute vis_sp for f=",ff)
                if mode_signal:
                    vis = Visibility_computor(self,freq=ff, Sky_sp=self.Sky_list[freq_id[i]]).compute_signal()
                else:
                    vis = Visibility_computor(self,freq=ff, Sky_sp=self.Sky_list[freq_id[i]]).compute()
                vis_sp[freq_id[i],:] = vis

            if mode_map:
                print("START compute vis_map for f=",ff)
                nside = hp.npix2nside(len(self.continuum_list[freq_id[i]]))
                print("nside used for continuum is :",nside)
                if mode_signal:
                    vis = Visibility_computor(self,freq=ff, skymap=self.continuum_list[freq_id[i]]).compute()
                else:
                    vis = Visibility_computor(self,freq=ff, skymap=self.continuum_list[freq_id[i]]).compute()
                vis_map[freq_id[i],:] = vis


        out = []
        if mode_signal:
            if mode_sp:
                self.signal_sp = vis_sp
                out.append(self.signal_sp)
            if mode_map:
                self.signal_map = vis_map
                out.append(self.signal_map)
        else: 
            if mode_sp:
                self.vis_sp = vis_sp
                out.append(self.vis_sp)
            if mode_map:
                self.vis_map = vis_map
                out.append(self.vis_map)

            if add_to_visibilities:
                vis_tot = np.zeros((dim0,dim1),dtype=np.complex128)*unit
                if mode_sp:
                    vis_tot += vis_sp
                if mode_map:
                    vis_tot += vis_map
                self.visibilities[freq_id] = vis_tot

        return out

            



    def add_extended_source(self, flux=1*u.Jy, pos=None, radius=None, compute_vis=False):
        if pos is None:
            ph = (np.random.rand()-0.5)*2*np.pi
            ss = (np.random.rand()-0.5)*2
            cc = np.sqrt(1-ss**2)
            source_pos = Position(pos=np.array([cc*np.cos(ph),cc*np.sin(ph),ss]))
        elif isinstance(pos,Position):
            source_pos = pos
        elif isinstance(pos, np.ndarray):
            source_pos = Position(pos=pos)
        else:
            raise TypeError('wrong input type for pos')

        if ((radius is None) or (radius=='sun')):
            # assume SUN
            sun_diameter = 696340*2*u.km # rf emission radius ????
            sun_earth_dist = 150e6*u.km 
            radius = sun_diameter/sun_earth_dist * u.rad
        elif radius=='earth':
            earth_diameter = 20000*u.km # 2*6371*u.km # EMISSION
            earth_moon_dist = 384400*u.km 
            radius = earth_diameter/earth_moon_dist
        elif isinstance(radius, float):
            radius = radius *u.rad
        elif isinstance(radius, u.Quantity):
            pass
        else:
            raise TypeError('wrong input type for radius')

        res = np.min(self.resol)

        Source_sky = source_extended(source_pos, res, flux, radius=radius)

        if compute_vis:
            if self.f.size==1:
                vis_source = Visibility_computor(self,freq=self.f, Sky_sp=Source_sky).compute()
            elif self.f.size>=2:
                vis_source = np.zeros((self.f.size,len(self.b)), dtype=np.complex128)
                for i in range(self.f.size):
                    vis_source[i,:] = Visibility_computor(self,freq=self.f[i], Sky_sp=Source_sky).compute().to_value(u.Jy)
                vis_source = vis_source*u.Jy
            else:
                raise TypeError('Interfero has no freq')

            out = (Source_sky, vis_source)
        else:
            out = Source_sky

        return out


    
    
        
        
        
    




            
      
        
      
        
class Visibility_computor(object):
    
    def __init__(self,Interfero,freq=None,Sky_sp=None,skymap=None,ant=False):
        
        self.b = Interfero.b
        self.n_sat = Interfero.n_sat
        self.satpos = Interfero.Swarm.satpos
        self.n_bl = self.b.shape[0]
        self.vis = np.zeros(self.n_bl,dtype=np.complex128)*u.Jy
        if ant is not False:
            self.vis_ant = np.zeros((self.n_bl,3),dtype=np.complex128)*u.Jy
        
        self.mode_sp = False
        self.mode_map = False
        
        if freq is None:
            if Interfero.k.size>=0:
                self.k = Interfero.k[0]
            else:
                raise TypeError('Interfero has no k (ie: no freq)')
        elif isinstance(freq, u.Quantity):
            if freq.size==1:
                self.k = 2*np.pi*freq/const.c
            elif freq.size>=2:
                self.k = 2*np.pi*freq[0]/const.c
            else:
                raise TypeError('freq has no size')
        elif isinstance(freq, float):
            self.k = 2*np.pi*freq*u.Hz/const.c
        elif isinstance(freq, int) or isinstance(freq, np.int64) or isinstance(freq, np.int32):
            self.freq_id = freq
            if Interfero.k.size>=0:
                if ( (freq<Interfero.k.size) and (freq>=0) ):
                    self.k = Interfero.k[freq]
                else:
                    self.k = Interfero.k[0]
                    print("WARNING, default freq is used: k=",self.k)
            else:
                raise TypeError('Interfero has no k (ie: no freq)')
        else:
            raise TypeError('wrong freq : float, u.Quantity, int (indices in list of freq)')

        #if freq=='1' or freq is None:
        #    self.k = Interfero.k0
        #elif freq=='2' or freq=='hf':
        #    self.k = Interfero.k_hf
        #elif type(freq) == u.Quantity or type(freq) == np.float128:
        #    self.k = 2*np.pi*freq/const.c
        #else:
        #    raise TypeError('wrong freq : str (1,2,hf)')
            
        if isinstance(Sky_sp, Sky):
            self.Sky = Sky_sp
            self.mode_sp = True   
        elif (Sky_sp is None) or (Sky_sp == False):
            pass
        elif Sky_sp == True:
            self.Sky = Interfero.Sky_list[0]
            self.mode_sp = True
        else:
            raise TypeError('wrong Sky input')
        
        if isinstance(skymap, u.Quantity):
            self.skymap = skymap.to_value(u.Jy)
            self.mode_map = True 
        elif isinstance(skymap, np.ndarray):
            self.skymap = skymap
            self.mode_map = True
        elif (skymap is None) or (skymap == False): 
            pass
        elif skymap == True:
            self.skymap = Interfero.continuum
            self.mode_map = True
        else:
            raise TypeError('wrong skymap input')
        
        if isinstance(ant, np.ndarray):
            self.use_ant = True
            self.ant_ori = ant
        elif ant == False:
            self.use_ant = False
        elif ant == True:
            self.ant_ori = Interfero.Swarm.ant_ori
            self.use_ant = True
        elif ant=='eee':
            self.ant_ori = np.array([[1,0,0],[0,1,0],[0,0,1]])
            self.use_ant = True
        else:
            raise TypeError('wrong use_ant input')
            
            
    def compute(self):
        
        
        if self.use_ant:            
            if len(np.shape(self.ant_ori))<=2:
                
                if self.mode_sp:
                    """
                    ns = self.Sky.ns
                    mrot_skypix = R.from_euler('yz',np.array(hp.vec2ang(self.Sky.source_pos)).T)
                    fluxes = self.Sky.fluxes #* np.exp(1j*np.random.rand(ns)*2*np.pi)
                    fieldE = np.array([1,1j,0] *ns ).reshape((ns,3))/np.sqrt(2) * np.sqrt(fluxes)[:,None]
                    fieldE_Ra = mrot_skypix.apply(fieldE)

                    vis_sp = np.dot(fieldE_Ra,self.ant_ori.T)
                    vis_sp *= np.conj(vis_sp)
                    """

                    print(self.Sky.source_pos)
                    print(self.ant_ori)
                    print(self.Sky.fluxes)

                    vis_sp = np.dot(self.Sky.source_pos, self.ant_ori)**2 * self.Sky.fluxes[:,None]
                    print(vis_sp.shape)
                    print(vis_sp)

                    phase = np.exp(-1j*self.k * np.dot(self.Sky.source_pos,self.b.T)).value
                    print(phase.shape)

                    vis_sp = np.sum(  vis_sp[:,None,:]  * phase[:,:,None]  ,axis=0)
                    print(vis_sp.shape)

                    #vis_sp *= u.Jy

                    self.vis_ant = vis_sp
                    
                    
                if self.mode_map:
                    npix = self.skymap.shape[0]
                    nside = hp.npix2nside(npix)
                    r_grid = np.array(hp.pix2vec(nside,np.arange(npix))).T
                    mrot_skypix = R.from_euler('yz',np.array(hp.vec2ang(r_grid)).T)
                    
                    fieldE = np.array([1,1j,0] *npix ).reshape((npix,3))/np.sqrt(2) * np.sqrt(self.skymap)[:,None]
                    fieldE_Ra = mrot_skypix.apply(fieldE)
                    
                    phase = np.exp(-1j*self.k * np.dot(r_grid,self.b.T)).value
                    
                    vis_map = np.dot(fieldE_Ra,self.ant_ori.T)
                    vis_map *= np.conj(vis_map)
                    vis_map = np.sum(  vis_map[:,:,None]  * phase[:,None,:]  ,axis=0).T

                    vis_map *= u.Jy
                    self.vis_ant += vis_map
                    
                
            elif len(np.shape(self.ant_ori))==3:
                pass #TODO
            else:
                raise TypeError('wrong ant_ori shape')
                
            out = self.vis_ant
        
        else:
            if self.mode_sp:

                #nsp = len(self.Sky.source_list)
                nsp = self.Sky.ns
                if nsp>0:
                    source_pos = da.from_array(self.Sky.source_pos, chunks=(10000))
                    
                    vis_sp_da = np.sum(  self.Sky.fluxes[:,None]  *  np.exp(-1j*np.dot(source_pos,((self.k*self.b).to_value(1)).T)),   axis=0)
                    with DaskProgressBar():
                        vis_sp = vis_sp_da.compute()
                
                #vis_sp *= u.Jy
                self.vis += vis_sp
                
                
            if self.mode_map:
                vis_map = np.zeros(self.n_bl,dtype=np.complex128)
                
                npix = self.skymap.shape[0]
                nside = hp.npix2nside(npix)
                #r_grid = np.array(hp.pix2vec(nside,np.arange(npix))).T
                r_grid = da.from_array(np.array(hp.pix2vec(nside,np.arange(npix))).T  , chunks=(10000))

                intensities = self.skymap *hp.nside2pixarea(hp.npix2nside(len(self.skymap)))# * 4###############################################""

                vis_map_da = np.sum(intensities[:,None] * np.exp(-1j* np.dot(r_grid,((self.k*self.b).T).to_value(1))), axis=0)
                with DaskProgressBar():
                #with Profiler() as prof, ResourceProfiler(dt=0.25) as rprof, CacheProfiler() as cprof, DaskProgressBar():
                    vis_map = vis_map_da.compute()
                #visualize([prof, rprof, cprof])
    
                vis_map *= u.Jy
                self.vis += vis_map
                
            out = self.vis
        
        return out    



    def compute_swht(self, lmax=None):
        t0 = time.time()

        if lmax is None:
            res = ((2*np.pi/self.k)/np.max(np.linalg.norm(self.b,axis=-1))).to_value(1)
            lmax = int(np.pi/res)*3
            print('lmax not mentionned, selected value is :',lmax)

        blm = hp.map2alm(self.skymap, lmax) #skymap*4 ###########################################################

        t1 = time.time()
        print(t1-t0, 'hp map2alm')

        b_cart = self.b.to_value(u.m)
        ang = np.array(hp.vec2ang(b_cart))
        ang[np.where(np.isnan(ang))] = 0
        r = np.linalg.norm(b_cart,axis=1)
        b_sph = np.zeros(b_cart.shape)
        b_sph[:,0] = r
        b_sph[:,1] = ang[0,:]
        b_sph[:,2] = ang[1,:] 

        t2 = time.time()
        print(t2-t1,'bsph') 

        l,m = hp.Alm.getlm(lmax)

        #harms = sph_harm(m[:,None],l[:,None], b_sph[None,:,2], b_sph[None,:,1])  
        #harms[np.where(np.isnan(harms))]=0
        #harms[np.where(np.isinf(harms))]=0

        #harms = sph_harm_sh(lmax,b_sph[:,1], b_sph[:,2]).T
        harms = da.from_array( sph_harm_sh(lmax,b_sph[:,1], b_sph[:,2]).T , chunks=(1000))

        t3 = time.time()
        print(t3-t2, 'harms')

        arr = 4*np.pi*(-1j)**l[:,None] * spherical_jn(l[:,None], self.k.to_value(1/u.m)*da.from_array(b_sph[None,:,0], chunks=(100))) * blm[:,None]* harms
        
        t4 = time.time()
        print(t4-t3, 'arr')


        vis_sph = np.sum(arr[:lmax+1,:],axis=0)
        vis_sph += 2*np.real(np.sum(arr[lmax+1:,:],axis=0))

        t5 = time.time()
        print(t5-t4,'vis_sph')

        with DaskProgressBar():
            vis_sph = vis_sph.compute() 

        vis_sph *= u.Jy
        self.vis += vis_sph

        return self.vis


    def compute_signal(self):

        signal = np.zeros(self.n_sat, dtype=np.complex128)

        if mode_sp:
            source_pos = da.from_array(self.Sky.source_pos, chunks=(10000))
            signal_sp_da = np.sum(  np.sqrt(self.Sky.fluxes[:,None])  *  np.exp(-1j*np.dot(source_pos,(self.k*self.satpos.T*u.m).to_value(1))),   axis=0)
            with DaskProgressBar():
                signal_sp = signal_sp_da.compute()
            signal += signal_sp


        if mode_map:
            npix = self.skymap.shape[0]
            nside = hp.npix2nside(npix)
            r_grid = da.from_array( np.array(hp.pix2vec(nside,np.arange(npix))).T , chunks=(10000) )

            intensities = self.skymap *hp.nside2pixarea(hp.npix2nside(len(self.skymap))) *u.Jy
            
            signal_map_da = np.sum(np.sqrt(intensities[:,None]) * np.exp(-1j* np.dot(r_grid,(self.k*self.satpos.T*u.m).to_value(1))),  axis=0)
            with DaskProgressBar():
                signal_map = signal_map_da.compute()
            signal += signal_map

        #jy12 = np.sqrt(1*u.Jy).unit
        #signal *= jy12
        self.signal = signal

        return self.signal






class Image_computer(object):

    def __init__(self, Interfero, vis=None, freq=None, baseline_mode=None, nside=None, indices=None):
        self.Interfero = Interfero

        if freq is None:
            if Interfero.k.size>=0:
                self.k = Interfero.k[0]
            else:
                raise TypeError('Interfero has no k (ie: no freq)')
        elif isinstance(freq, u.Quantity):
            if freq.size==1:
                self.k = 2*np.pi*freq/const.c
            else:
                self.k = 2*np.pi*freq[0]/const.c
                print("WARNING, default freq is used: k=",self.k)
        elif isinstance(freq, float):
            self.k = 2*np.pi*freq*u.Hz/const.c
        elif isinstance(freq, int) or isinstance(freq, np.int64) or isinstance(freq, np.int32):
            if Interfero.k.size>=0:
                if ( (freq<Interfero.k.size) and (freq>=0) ):
                    self.k = Interfero.k[freq]
                else:
                    self.k = Interfero.k[0]
                    print("WARNING, default freq is used: k=",self.k)
            else:
                raise TypeError('Interfero has no k (ie: no freq)')
        else:
            raise TypeError('wrong freq : float, u.Quantity, int (indices in list of freq)')

        #self.k =  2*np.pi*self.f/const.c

        if vis is not None:
            self.vis = vis
        else:
            if isinstance(freq, int) or isinstance(freq, np.int64) or isinstance(freq, np.int32):
                if Interfero.f.size == 1:
                    self.vis = Interfero.visibilities
                    print("WARNING, default vis is used")
                elif Interfero.f.size >= 2:
                    if ( (freq<Interfero.f.size) and (freq>=0) ):
                        self.vis = Interfero.visibilities[freq]
                    else:
                        self.vis = Interfero.visibilities[0]
                        print("WARNING, default vis is used")
                else:
                    raise TypeError('Interfero has no freq')
            else:
                raise TypeError('please input a visibility')

        if baseline_mode is not None:
            self.b = baseline_mode
        else:
            self.b = Interfero.b

        if nside is not None:
            self.nside = nside
        else:
            resol = 2*np.pi/(self.k*Interfero.d_max).to_value(1)
            self.nside = 2**int(np.ceil(np.log(4*np.pi/12/resol)/np.log(2)))
        self.npix = hp.nside2npix(self.nside)

        if indices is not None:
            self.indices = indices
        else:
            self.indices = np.arange(self.npix)



    def DFT(self, mode=None): 

        r_grid = da.from_array( np.array(hp.pix2vec(self.nside,self.indices)).T , chunks=(10000))
        
        mapp_da = np.mean(np.real(self.vis.value[:,None] * np.exp((1j*np.dot((self.b*self.k).to_value(1),r_grid.T)))),axis=0)
        with DaskProgressBar():
            mapp_list = mapp_da.compute()

        if mode == 'list':
            out = mapp_list

        else:
            mapp = np.zeros(self.npix,dtype=np.float64)*np.nan
            mapp[self.indices] = mapp_list
            out = mapp

        return out

    def DFT_point(self, vec):
        flux = np.mean(self.vis.value * np.exp(1j*np.dot((self.k*self.b).to_value(1),vec)),axis=0).real
        return flux

    def beam(self, signal=None, mode=None):
        if signal is not None:
            self.signal = signal
        else:
            self.signal = self.Interfero.signal
        satpos = self.Interfero.Swarm.satpos

        r_grid = da.from_array( np.array(hp.pix2vec(self.nside,self.indices)).T , chunks=(10000) )     

        mapp_da = np.abs(np.mean(self.signal[:,None] * np.exp(1j*np.dot((self.k*satpos*u.m).to_value(1),r_grid.T)),axis=0))
        with DaskProgressBar():
            mapp_list = mapp_da.compute()

        if mode == 'list':
            out = mapp_list

        else:
            mapp = np.zeros(self.npix,dtype=np.complex128)*np.nan
            mapp[self.indices] = mapp_list
            out = mapp

        return out


    def SWHT(self, lmax=None):
        if lmax is not None:
            self.lmax = lmax
        else:
            self.lmax = int(np.pi/self.Interfero.resol)*3
        
        ang = np.array(hp.vec2ang(self.b.value))
        ang[np.where(np.isnan(ang))] = 0
        r = np.linalg.norm(self.b.value,axis=1)
        b_sph = np.zeros(self.b.shape)
        b_sph[:,0] = r
        b_sph[:,1] = ang[0,:]
        b_sph[:,2] = ang[1,:]
        
        blm = blm_all(lmax,vis,b_sph,k)

        mapp = hp.sphtfunc.alm2map(blm, self.nside)

        return mapp

    def FFT3D(self):
        bl = (self.b*self.k/(2*np.pi)).value
        vis_grid, dl = grid3d(bl, self.vis, nside=self.nside)
        mapp = make_mapp_fft3d_bis(vis_grid, self.nside, dl)

        #n_mesh = 301
        #vis_grid = grid3d(bl, vis, n_mesh)
        #mapp = make_mapp_fft3d(vis_grid, self.nside)

        return mapp



#class Image_diplayer(object):

#    def __init__(self):

#    def hpmap(mapp)

#    def 








class Source_finder_double_freq(object):

    def __init__(self, Interfero, freq_lf=None, freq_hf=None, vis_lf=None, vis_hf=None, display=False, verbose=False, save_file=None, apply_mask=True):
        self.Sky_lf = Sky()
        self.Sky_hf = Sky()
        self.sources_params = []
        self.Interfero = Interfero
        self.display = display
        self.verbose = verbose
        if save_file is not None:
            self.save_file = save_file
            self.save_fig = True
            self.display = True
        else:
            self.save_fig = False


        self.initialize_freq(freq_lf=freq_lf, freq_hf=freq_hf, vis_lf=vis_lf, vis_hf=vis_hf)

        self.radius_hf = self.resol_lf *5*2
        self.radius_hf2 = self.resol_hf *5

        # initialize masks
        self.apply_mask = apply_mask
        if self.apply_mask:
            self.mask_lf = np.array([],dtype=np.int32)
            self.mask_hf = np.array([],dtype=np.int32)
            self.mask_hf2 = np.array([],dtype=np.int32)

    def set_display(self, display):
        self.display = display

    def set_verbose(self, verbose):
        self.verbose = verbose

    def set_save_file(self, save_file):
        if save_file is not None:
            self.save_file = save_file
            self.save_fig = True
            self.display = True

    def set_mask_radius(self, mask_radius=10/180*np.pi*u.rad):
        if isinstance(mask_radius, u.Quantity):
            self.mask_radius = mask_radius
        elif isinstance(mask_radius, np.float64):
            self.mask_radius = mask_radius * u.rad
        else:
            raise TypeError('wrong type for input mask_radius')

    def disable_masks(self):
        self.apply_mask = False

    def clear_found_sky(self):
        self.Sky_lf = Sky()
        self.Sky_hf = Sky()

    def initialize_freq(self, freq_lf=None, freq_hf=None, vis_lf=None, vis_hf=None):
        # handle input freq_lf 
        if freq_lf is None:
            if isinstance(self.Interfero.f, u.Quantity):
                if self.Interfero.f.size>=2:
                    self.freq_lf = self.Interfero.f[0]
                    self.id_freq_lf = 0
                elif self.Interfero.f.size==1:
                    self.freq_lf = self.Interfero.f
                else:
                    raise TypeError('No low frequency')
            else:
                raise TypeError('Interfero do not have a frequency u.Quantity')
        elif isinstance(freq_lf, int) or isinstance(freq_lf, np.int64) or isinstance(freq_lf, np.int32):
            if isinstance(self.Interfero.f, u.Quantity):
                if ( (freq_lf<len(self.Interfero.f)) and (freq_lf>=0) ):
                    self.id_freq_lf = freq_lf
                    self.freq_lf = self.Interfero.f[freq_lf]
                else:
                    raise TypeError('freq_lf: integer too high; not enough value in Interfero.f')
            else:
                raise TypeError('Interfero do not have a frequency u.Quantity')
        elif isinstance(freq_lf, np.float64):
            self.freq_lf = freq_lf * u.Hz
        elif isinstance(freq_lf, u.Quantity):
            self.freq_lf = freq_lf
        else:
            raise TypeError('wrong input type for freq_lf (None, u.Quantity, np.float64, int')

        # handle input freq_hf 
        if freq_hf is None:
            if isinstance(self.Interfero.f, u.Quantity):
                if self.Interfero.f.size>1:
                    self.freq_hf = self.Interfero.f[1]
                    self.id_freq_hf = 1
                else:
                    raise TypeError('No high frequency')
            else:
                raise TypeError('Interfero do not have a frequency of type u.Quantity')
        elif isinstance(freq_hf, u.Quantity):
            self.freq_hf = freq_hf
        elif isinstance(freq_hf, np.float64):
            self.freq_hf = freq_hf * u.Hz
        elif isinstance(freq_hf, int) or isinstance(freq_hf, np.int64) or isinstance(freq_hf, np.int32):
            if isinstance(self.Interfero.f, u.Quantity):
                if ( (freq_hf<len(self.Interfero.f)) and (freq_hf>=0) ):
                    self.id_freq_hf = freq_hf
                    self.freq_hf = self.Interfero.f[freq_hf]
                else:
                    raise TypeError('freq_hf: integer too high; not enough value in Interfero.f')
            else:
                raise TypeError('Interfero do not have a frequency u.Quantity')
        else:
            raise TypeError('wrong input type for freq_hf (None, u.Quantity, np.float64, int')

        # handle input vis_lf 
        if vis_lf is None:
            if hasattr(self.Interfero, 'visibilities'):
                if hasattr(self, 'id_freq_lf'):
                    self.vis_lf = np.copy(self.Interfero.visibilities[self.id_freq_lf])
                elif self.Interfero.visibilities.size>1:
                    self.vis_lf = np.copy(self.Interfero.visibilities[0])
                elif self.Interfero.visibilities.size==1:
                    self.vis_lf = np.copy(self.Interfero.visibilities)
                else:
                    raise TypeError('Interfero.visibilities is empty')
            else:
                raise TypeError('Interfero do not have visibilities')
        elif isinstance(vis_lf, u.Quantity):
            self.vis_lf = vis_lf
        elif isinstance(vis_lf, np.float64):
            self.vis_lf = vis_lf * u.Jy
        else:
            raise TypeError('wrong input type for vis_lf (None, u.Quantity, np.float64')

        # handle input vis_hf 
        if vis_hf is None:
            if hasattr(self.Interfero, 'visibilities'):
                if hasattr(self, 'id_freq_hf'):
                    self.vis_hf = np.copy(self.Interfero.visibilities[self.id_freq_hf])
                elif self.Interfero.visibilities.size>2:
                    self.vis_hf = np.copy(self.Interfero.visibilities[1])
                else:
                    raise TypeError('no indices matches in Interfero.visibilities for high frequency')
            else:
                raise TypeError('Interfero do not have visibilities')
        elif isinstance(vis_hf, u.Quantity):
            self.vis_hf = vis_hf
        elif isinstance(vis_hf, np.float64):
            self.vis_hf = vis_hf * u.Jy
        else:
            raise TypeError('wrong input type for vis_hf (None, u.Quantity, np.float64')

        # get resolutions
        if ( (self.Interfero.resol.size>1) and (hasattr(self, 'id_freq_lf')) ):
            self.resol_lf = self.Interfero.resol[self.id_freq_lf]
        else:
            self.resol_lf = (const.c/self.freq_lf/self.Interfero.Swarm.d_max  *u.rad).to(u.rad)

        if ( (self.Interfero.resol.size>1) and (hasattr(self, 'id_freq_hf')) ):
            self.resol_hf = self.Interfero.resol[self.id_freq_hf]
        else:
            self.resol_hf = const.c/self.freq_hf/self.Interfero.Swarm.d_max   *u.rad

        # compute nsides
        self.nside_lf = 2**int(np.ceil(np.log(4*np.pi/12/(self.resol_lf.to_value(u.rad)/2))/np.log(2))) # factor 2
        self.nside_hf = 2**int(np.ceil(np.log(4*np.pi/12/(self.resol_hf.to_value(u.rad)/2))/np.log(2))) # factor 2
        self.nside_hf2 = 2**int(np.ceil(np.log(4*np.pi/12/(self.resol_hf.to_value(u.rad)/10))/np.log(2))) # factor 10


    def update_masks(self, Center=None, radius=None):
        if Center is None:
            vec = self.Sky_hf[-1].vec
        elif isinstance(Center, Source):
            vec = Center.vec
        elif isinstance(Center, np.ndarray):
            vec = Center
        else:
            raise TypeError ('wrong input type for Center')

        if radius is None:
            radius = self.mask_radius
            
        indices_disc_lf = hp.query_disc(self.nside_lf, vec, radius=radius.to_value(u.rad))
        self.mask_lf = np.concatenate((self.mask_lf,indices_disc_lf))

        indices_disc_hf = hp.query_disc(self.nside_hf, vec, radius=radius.to_value(u.rad))
        self.mask_hf = np.concatenate((self.mask_hf,indices_disc_hf))

        indices_disc_hf2 = hp.query_disc(self.nside_hf2, vec, radius=radius.to_value(u.rad))
        self.mask_hf2 = np.concatenate((self.mask_hf2,indices_disc_hf2))


    def update_vis(self,Source_hf=None, Source_lf=None):
        if Source_hf is None:
            Sky_source_hf = Sky(source_list=[self.Sky_hf[-1]])
        elif isinstance(Source_hf, Source):
            Sky_source_hf = Sky()
            Sky_source_hf.add_source(Source=Source_hf)
        else:
            raise TypeError ('wrong input Source_hf')

        if Source_lf is None:
            if Sky_lf.source_list.size==0:
                Sky_source_lf = Sky_source_hf
            else:
                Sky_source_lf = Sky(source_list=[self.Sky_lf[-1]])
        elif isinstance(Source_lf, Source):
            Sky_source_lf = Sky()
            Sky_source_lf.add_source(Source=Source_lf)
        else:
            raise TypeError ('wrong input Source_lf')


        vis_source_lf  = Visibility_computor(self.Interfero,freq=self.freq_lf,Sky_sp=Sky_source_lf).compute()

        vis_source_hf  = Visibility_computor(self.Interfero,freq=self.freq_hf,Sky_sp=Sky_source_hf).compute()

        self.vis_lf -= vis_source_lf
        self.vis_hf -= vis_source_hf



    def get_sources(self, it_max=20):
        for it in range(it_max):
            print("\n ITERATION nb: ",it+1)

            Source_hf, success_params = self.find_source()

            flux_lf = Image_computer(self.Interfero, vis=self.vis_lf, freq=self.freq_lf).DFT_point(Source_hf.vec)
            Source_lf = Source(Coord=Source_hf.Coord, flux=flux_lf)
            if self.verbose:
                print(flux_lf)

            if success_params[-1]:
                self.Sky_hf.add_source(Source=Source_hf)
                self.Sky_lf.add_source(Source=Source_lf)
                self.sources_params.append(success_params)


            if self.apply_mask:
                self.update_masks(Center=Source_hf)
            
            self.update_vis(Source_hf=Source_hf, Source_lf=Source_lf)

        return self.Sky_hf, np.array(self.sources_params)



    def find_source(self):
        ### FULL SKY IMAGE
        if self.verbose:
            print('Computing img lf' )
        self.mapp_lf = Image_computer(self.Interfero, vis=self.vis_lf, freq=self.freq_lf, nside=self.nside_lf).DFT()
        if self.apply_mask:
            self.mapp_lf[self.mask_lf] = np.nan 
        idmax_lf = np.nanargmax(self.mapp_lf)
        flux_lf = self.mapp_lf[idmax_lf]
        pos_lf = np.array(hp.pix2vec(self.nside_lf, idmax_lf))
        if self.verbose:
            print(idmax_lf, flux_lf, pos_lf)

        if self.display:
            hp.mollview(self.mapp_lf, title=' ', unit='Brightness (Jy)')
            hp.projscatter(hp.vec2ang(pos_lf),marker='x',color='r')
            #plt.show()
            if self.save_fig:
                file_name = self.save_file + 'img_f1.png'
                plt.savefig(file_name)

        ### FIRST ROI
        if self.verbose:
            print('Computing img hf')
        radius_hf = 3*self.resol_lf.to_value(u.rad)
        indices_disc_hf = hp.query_disc(self.nside_hf, pos_lf, radius=radius_hf)
        self.mapp_hf = Image_computer(self.Interfero, vis=self.vis_hf, freq=self.freq_hf, indices=indices_disc_hf, nside=self.nside_hf).DFT()
        if self.apply_mask:
            self.mapp_hf[self.mask_hf] = np.nan 
        idmax_hf = np.nanargmax(self.mapp_hf)
        pos_hf = np.array(hp.pix2vec(self.nside_hf, idmax_hf))
        if self.verbose:
            print(idmax_hf, pos_hf)

        if self.display:
            xsize=200
            reso = int(radius_hf*2*180/np.pi*60/xsize)
            tt,pp = hp.vec2ang(pos_hf)
            tt0,pp0 = hp.vec2ang(pos_lf)
            hp.gnomview(self.mapp_hf,reso=reso,xsize=xsize,rot =(pp0*180/np.pi,90-tt0*180/np.pi,0), title=' ', unit='Brightness (Jy)')
            hp.projscatter(tt,pp,c='red',marker='+',label='fit')
            hp.projscatter(tt0,pp0,c='white',marker='x',label='original guess')
            plt.legend()

            #hp.mollview(self.mapp_hf)
            #hp.projscatter(hp.vec2ang(pos_hf),marker='x',color='r')
            ##plt.show()

            if self.save_fig:
                file_name = self.save_file + 'img_f2.png'
                plt.savefig(file_name)

        ### SECOND ROI
        if self.verbose:
            print('Computing pixlist')
        radius_hf2 = 3 * self.resol_hf.to_value(u.rad)
        indices_disc_hf2 = hp.query_disc(self.nside_hf2, pos_hf, radius=radius_hf2)
        indices = indices_disc_hf2[np.isin(indices_disc_hf2, self.mask_hf2,invert=True)]
        self.pix_list_hf2 = Image_computer(self.Interfero, vis=self.vis_hf, freq=self.freq_hf, indices=indices, nside=self.nside_hf2).DFT(mode='list')
        pos_hf2 = hp.pix2vec(self.nside_hf2, indices[np.nanargmax(self.pix_list_hf2)])
        guess = Position(pos=pos_hf2)

        if self.save_fig:
            file_name = self.save_file+'img_f2_zoom.png'
        else:
            file_name = None

        ### GAUSSIAN FIT
        Fit = Fit_gauss(self.resol_hf, display=self.display,save_file=file_name, verbose=self.verbose)
        Fit.prepare_data_from_list(self.pix_list_hf2, indices, self.nside_hf2, guess)
        res = Fit.run_fit(auto_setup=True)

        Source_found = Fit.Source_fit.copy() #Source(Coord=Position(ang=np.array([res.x[1],res.x[2]])), flux=res.x[0])
        success_params = Fit.success_params

        return Source_found, success_params #res.success




        
    

class Fit_gauss(object):
    """
    Fit a source with a 2DGaussian
    """
    
    def __init__(self, resol, display=False, save_file=None, verbose=False):

        self.resol = resol

        self.display = display
        
        self.save_file = save_file

        self.verbose = verbose


    def prepare_data_from_map(self, mapp, Pos, mask, nside, radius):

        self.nside = nside
        self.Pos_g = Pos
        if isinstance(radius, u.Quantity):
            radius = radius.to_value(u.rad)
        self.radius = radius #=resol*10
        disc = hp.query_disc(nside, self.Pos_g.vec, self.radius)
        self.indices = disc[np.isin(disc,mask,invert=True)]
        
        data = mapp[self.indices]
        maxx = np.max(data)
        self.min_data = np.min(data)
        data -= self.min_data
        self.factor = np.max(data)
        data /= self.factor
        self.data = data
        
        if self.verbose:
            print("data")
            print(self.min_data)
            print(self.factor,maxx)

    def prepare_data_from_list(self, pix_list, indices, nside, Pos):
        self.nside = nside
        self.indices = indices
        self.Pos_g = Pos

        data = pix_list
        maxx = np.max(data)
        self.min_data = np.min(data)
        data -= self.min_data
        self.factor = np.max(data)
        data /= self.factor
        self.data = data

        self.radius = hp.nside2resol(nside)*10*5

        if self.verbose:
            print("data")
            print(self.min_data)
            print(self.factor,maxx)

        
    def set_p0(self, amp=None, Pos=None, sig=None, bkg=None):
        if Pos is None:
            Pos = self.Pos_g
        elif Pos=='max':
            imax = self.indices[np.argmax(self.data)]
            Pos = Position(ang=hp.pix2ang(self.nside,imax)*u.rad)   
        if sig is None:
            sig = self.radius/5
        if bkg is None:
            bkg = 0
        if amp is None:
            amp = 1-bkg
        
        theta_g, phi_g = Pos.ang.value
        
        self.p0 = np.array([amp,theta_g,phi_g,sig,bkg])
        
    
    def setup_boundary(self):
        theta_g = self.Pos_g.ang.value[0]
        phi_g = self.Pos_g.ang.value[1]
        self.bnds = ([0, theta_g-self.radius/4, phi_g-self.radius/4, 1e-10,0],
                     [1,theta_g+self.radius/4,phi_g+self.radius/4,self.radius,1])
    
    
        
    def run_fit(self, auto_setup=False):
        if auto_setup:
            self.set_p0()
            self.setup_boundary()
        
        self.errorfunction = lambda p: np.ravel(self.data -  gaussian_healpix2(*p,self.nside)(self.indices) )
    
        res = optimize.least_squares(self.errorfunction, self.p0, bounds = self.bnds)
        
        self.validate_fit(res)

        p = res.x
        flux_fit, theta_fit, phi_fit, sig_fit, bkg_fit = res.x
        
        ### correct the result: reverse resampling + angles in range
        flux_fit *= self.factor
        #p[0] += self.min_data
        theta_fit = theta_fit%(np.pi)
        phi_fit = phi_fit%(2*np.pi)

        self.Source_fit = Source(Position(ang=np.array([theta_fit, phi_fit])),flux=flux_fit)
        
        if self.verbose:
            print("fit parameters ", res.x)

        if self.display:
            
            if self.nside<2**10:
                mapp_disp = np.zeros(hp.nside2npix(self.nside))
                mapp_disp[self.indices] += self.data

                reso = int(self.radius*3*180/np.pi*60/200)
                map_center = (res.x[2]*180/np.pi,90-res.x[1]*180/np.pi,0)
                hp.gnomview(mapp_disp,reso=reso,xsize=200,rot =map_center,unit="normalized brightness",title=' ') #,title="Portion of the sky where the fit is performed"
                hp.projscatter(res.x[1],res.x[2],c='red',marker='+',label='fit')
                hp.projscatter(self.Pos_g.ang,c='white',marker='x',label='original guess')
                plt.legend()
                
                if self.save_file is not None:
                    print("A figure is saved: Gauss fit")
                    plt.savefig(self.save_file)

            else:
                print('pixlist 2 radec map')
                Pos_fit = Position(ang=np.array([res.x[1],res.x[2]]))
                mapp_disp, pos_fit = pixlist2radecmap(self.data,self.indices,self.nside,self.Pos_g.vec,self.radius*1.2,pos_fit=Pos_fit)
                plt.figure()
                plt.title(' ')
                plt.imshow(mapp_disp)
                plt.colorbar(label="normalized brightness")
                plt.scatter(pos_fit[1],pos_fit[0],color='r',marker='x',label='fit')
                plt.legend()
                if self.save_file is not None:
                    print("A figure is saved: Gauss fit with high nside")
                    plt.savefig(self.save_file)
        
        
        self.res = res
        return self.res
    
    
    def validate_fit(self,res):
        diff_fit = res.cost*2/len(res.fun)

        amp_bool = res.x[0] > 0.5
        sig_bool = res.x[3] < self.resol.to_value(u.rad)#*1.5
        diff_bool = diff_fit < 0.1
        self.success_bools = np.array([res.success, amp_bool, sig_bool, diff_bool])
        self.success = bool(np.prod(self.success_bools))
        self.success_params = np.array([res.success, res.x[0], res.x[3], diff_fit, self.success])

        if self.verbose:
            print("Success : ",self.success, "idividual bool", self.success_bools)
            print("params :", self.success_params)
            print("ref value :",0.5 , self.resol, 0.1)
        return None
        
        
     
  
    
        
        
        
        
        
class Rot_finder(object):
    """
    Object that enable to compute the pattern match between two sets of directions and the transform that links them.
    """
    
    def __init__(self, s_cat, s_mes, beam_size, auto_setup=True, verbose=False):
        """
        s_cat : Sky object listing the catalog sources considered
        s_mes : Sky object listing the measured sources
        beam_size : in u.rad, the expected angular distance error made between the measured direction and true direction, generally, the resolution at nu_2
        """
        
        self.s_cat = s_cat.copy()
        self.s_mes = s_mes.copy()
        self.beam_size = beam_size
        self.verbose = verbose
        if auto_setup:
            self.gather_sources()
            self.make_tables(idx='1') 
            self.make_tables(idx='2')  
            self.compute_permut(self.beam_size)


    def set_beam_size(self, beam_size):
        self.beam_size = beam_size    

    def gather_sources(self):
        """
        beam_size is used to define the gathering radius
        """
        self.s_cat = self.s_cat.gather_sources(self.beam_size)
        #self.s_mes = self.s_mes.gather_sources(self.beam_size)
        
    def make_tables(self, idx='1'):
        """
        builds the table of the geometrical parameters of every triangles of each source list in order to compute the pattern match afterwise.
        """
        
        # select the source list
        if idx=='1':
            sl=self.s_cat
        else:
            sl=self.s_mes
        
        # build the table
        ns = sl.ns
        table_prop = []
        table_idx = []
        for i in range(ns):
            for j in range(ns-1):
                if j==i:
                    continue
                else:
                    for k in range(j+1,ns):
                        if k==j or k==i:
                            continue
                        else:
                            table_prop.append([ sl.angle_between(i,j).to_value(u.rad),  sl.angle_between(i,k).to_value(u.rad), sl.angle_3source(i,j,k).to_value(u.rad) ])                     
                            #table_prop.append([ sl[i].angle_between(sl[j]).to_value(u.rad),  sl[i].angle_between(sl[k]).to_value(u.rad), sl[i].angle_3source(sl[j],sl[k]).to_value(u.rad) ])                     
                            table_idx.append(i)
        table_prop = np.array(table_prop).reshape((-1,3)) * u.rad
        table_idx = np.array(table_idx)

        # save result into attributes
        if idx=='1':
            self.t_cat = table_prop
            self.t_cat_id = table_idx
        else:
            self.t_mes = table_prop
            self.t_mes_id = table_idx

        
        
    def compute_permut(self,eps):
        """
        performs the pattern match 
            verifies the matching parameters per pair of triangles
            counts the number of vote per pair of sources
            match the sources
            sort the lists
        """
        diff = np.abs(self.t_cat[:,None,:]-self.t_mes[None,:,:])
        self.diff = diff

        #eps = beam_size
        eps_a = eps
        eps_b = eps
        eps_gamma = eps*2
        
        bool_a = diff[:,:,0]<eps_a
        bool_b = diff[:,:,1]<eps_b
        #bool_gamma = diff[:,:,2]<eps_gamma
        #bool_gamma = diff[:,:,2]* ( 1/(1/self.tp1[:,None,0].value+1/self.tp1[:,None,1].value)/2 + 1/(1/self.tp2[None,:,0].value+1/self.tp2[None,:,1].value)/2 )  < eps
        bool_gamma = diff[:,:,2] / (1/self.t_cat[:,None,0].to_value(u.rad) + 1/self.t_cat[:,None,1].to_value(u.rad)) < eps_gamma
        
        bool_tot = bool_a * bool_b * bool_gamma
        self.bool_tot = bool_tot
        
        ns_cat = self.s_cat.ns
        ns_mes = self.s_mes.ns
        ns_max = min(ns_cat, ns_mes)
        dn1 = int((ns_cat-1)*(ns_cat-2) /2)
        dn2 = int((ns_mes-1)*(ns_mes-2) /2)

        permut = np.zeros((ns_cat,ns_mes))
        for i in range(ns_cat):
            for j in range(ns_mes):
                permut[i,j] = np.sum(bool_tot[i*dn1:(i+1)*dn1, j*dn2:(j+1)*dn2])
                
        self.permut = permut
        
        # Select the matching indexes
        # min_vote is used as a threshold on the number of vote, then for each measured sources is assigned a catalog source, the one that shares the most votes with
        min_vote = np.max([3, np.mean(permut) + 3*np.std(permut)])
        #min_vote = np.max([1.0, (ns_max/2-1)*(ns_max/2-2)/2]) # old version, assumes at least 50% of measured sources have a match in the catalog
        col = np.where(np.max(permut,axis=1) >= min_vote)[0]
        idx_sort = np.argmax(permut[col,:],axis=1)
        
        self.col = col
        self.idx_sort = idx_sort
        
        if self.verbose:
            print('min_vote',min_vote)
            print('col',col)
            print('idx_sort',idx_sort)
        
        # list the matching sources in order
        self.s_cat_sort = Sky(source_pos=np.copy(self.s_cat.source_pos[col,:])
                            ,fluxes=np.copy(self.s_cat.fluxes[col]))
        
        self.s_mes_sort = Sky(source_pos=np.copy(self.s_mes.source_pos[idx_sort,:])
                            ,fluxes=np.copy(self.s_mes.fluxes[idx_sort]))

            
        
    def compute_rotation(self):
        """
        Compute the rotation based on the list of matching sources
        """

        def err_func(ang,cart1,cart2,weights=None, flip=False):
            """error function"""
            if flip:
                cart1 = cart1*np.array([1,1,-1])
            mrot = R.from_euler('zyx',ang).inv()
            cart2_rot = mrot.apply(cart2)
            #err = np.sum((cart1-cart2_rot)**2,axis=1)
            err = np.arccos(np.sum(cart1*cart2_rot,axis=-1))
            err[np.where(np.isnan(err))] = 0
            if weights is not None:
                err *= np.sqrt(weights)
            return err

        # The function requires at least 3 sources to compute a transform
        if (self.s_cat_sort.ns<3) or (self.s_mes_sort.ns<3):
            raise TypeError('Not enough match to compute a rotation ')
        
        # The best rotation is computed twice. In case there is a reflection or not.
        error_function = lambda p : err_func(p,self.s_cat_sort.source_pos, self.s_mes_sort.source_pos, weights=self.s_cat_sort.fluxes.to_value(u.Jy))
        error_function_f = lambda p : err_func(p,self.s_cat_sort.source_pos, self.s_mes_sort.source_pos, weights=self.s_cat_sort.fluxes.to_value(u.Jy),flip=True)

        p0 = [0,0,0]
        p_fit, success = optimize.leastsq(error_function, p0)
        p_fit_f, success_f = optimize.leastsq(error_function_f, p0)

        err = error_function(p_fit)
        err_f = error_function_f(p_fit_f)

        if self.verbose:
            print('mean error and pfit',np.mean(err**2), p_fit)
            print('mean error and pfit with FLIP',np.mean(err_f**2), p_fit_f)

        if np.mean(err**2) <= np.mean(err_f**2):
            self.flip = False
            self.m_rot_fit = R.from_euler('zyx',p_fit)
        else:
            self.flip = True
            self.m_rot_fit = R.from_euler('zyx',p_fit_f)

        self.q_fit = self.m_rot_fit.as_quat()
    
    
        
    def run(self,beam_size,eps=None):
        self.set_beam_size(beam_size)
        if eps is None:
            eps = self.beam_size
        
        self.gather_sources()
        self.make_tables(idx='1')
        self.make_tables(idx='2')
        self.compute_permut(eps)
        self.compute_rotation()
        
        return self.m_rot_fit, self.flip
        
        
        
        
        
































def cart2sph(a_cart,noR=False):
    r_a = np.sqrt(a_cart[:,0]**2+a_cart[:,1]**2+a_cart[:,2]**2)
    phi_a = np.arctan2(a_cart[:,1],a_cart[:,0])
    theta_a = np.arccos(a_cart[:,2]/r_a)
    theta_a[np.where(np.isnan(theta_a))] = 0 # convention for the point (0,0,0) in cartesian
    
    a_sph = np.transpose(np.array([r_a,theta_a,phi_a]))
    if noR:
        a_sph = a_sph[:,1:]
    return a_sph


def sph2cart(a_sph):
    hasR = a_sph.shape[1]==3
    
    x=np.sin(a_sph[:,0+hasR])*np.cos(a_sph[:,1+hasR])
    y=np.sin(a_sph[:,0+hasR])*np.sin(a_sph[:,1+hasR])
    z=np.cos(a_sph[:,0+hasR])
    a_cart = np.transpose(np.array([x,y,z]))
    if hasR:
        a_cart *= a_sph[:,0]
    return a_cart





def blm_2D(l,m,vis,b_sph,k0):
    """
    OLD VERSION
    compute the blm coefficients of the spherical harmonic decomposition of the brightness
    l = 2D array dim(N_baselines)xdim(lm)
    where N_baselines = len(Vis)
    m same
    az, el, r = 2D array of same dimension as l and m
              = spherical coordinates of the baselines (azimuth, elevation, radius)=(phi,theta,r)
    """
    lA,az = np.meshgrid(l,b_sph[:,2])
    mA,el = np.meshgrid(m,b_sph[:,1])
    lA,r  = np.meshgrid(l,b_sph[:,0])
    
    vlm = 2*k0**2/np.pi * np.sum( np.transpose(np.tile(vis,(len(l),1))) * spherical_jn(lA,k0*r) * sph_harm(mA, lA, az, el) ,axis=0)
    blm = vlm/(4*np.pi*(-1j)**l)
    return blm


def blm_calc(l,m,vis,b_sph,k):
    """
    OLD VERSION
    compute the blm coefficients of the spherical harmonic decomposition of the brightness
    """
    if isinstance(k, u.Quantity):
        k = k.to_value(1/u.m)
    if isinstance(vis, u.Quantity):
        vis = vis.to_value(u.Jy)

    harms = sph_harm(m[None,:], l[None,:], b_sph[:,None,2], b_sph[:,None,1])
    harms[np.where(np.isnan(harms))] = 0
    harms[np.where(np.isinf(harms))] = 0

    bessel = spherical_jn(l[None,:],k*b_sph[:,0,None])
    vlm = 2*k**2/np.pi * np.sum( vis[:,None] * bessel * harms ,axis=0)
    blm = vlm/(4*np.pi*(-1j)**l)
    return blm





   
def compute_image_dft(vis, b, freq, nside, indices=None):
    """
    OLD VERSION
    """
    npix = hp.nside2npix(nside)
    mapp = np.zeros(npix,dtype=np.complex128)
    if indices is None:
        indices = np.arange(npix)   
    r_grid = np.array(hp.pix2vec(nside,indices)).T
    
    k = 2*np.pi*freq/const.c
        
    for i,e in enumerate(indices):
        mapp[e] = np.mean(vis.value * np.exp(1j*k*np.dot(r_grid[i,:],b.T)))
    mapp = mapp.real
    
    return mapp


def compute_image_beam(signal, satpos, freq, nside, indices=None):
    """
    OLD VERSION
    """
    npix = hp.nside2npix(nside)
    mapp = np.zeros(npix,dtype=np.complex128)
    if indices is None:
        indices = np.arange(npix)   
    r_grid = np.array(hp.pix2vec(nside,indices)).T
    
    k = 2*np.pi*freq/const.c
    
    for i,e in enumerate(indices):
        mapp[e] = np.mean(signal.value * np.exp(1j*k*np.dot(r_grid[i,:],satpos.T*u.m)))
    mapp = (mapp*np.conj(mapp)).real
    
    return mapp


def compute_image_swht(vis, b, lmax, freq, nside, indices=None):
    """
    OLD VERSION
    """
    k = 2*np.pi*freq/const.c
    
    l,m= hp.Alm.getlm(lmax)
        
    ang = np.array(hp.vec2ang(b.value))
    ang[np.where(np.isnan(ang))] = 0
    r = np.linalg.norm(b.value,axis=1)
    b_sph = np.zeros(b.shape)
    b_sph[:,0] = r
    b_sph[:,1] = ang[0,:]
    b_sph[:,2] = -ang[1,:]
    
    blm = blm_2D(l,m,vis.value,b_sph,k.value)
    print(blm)
        
    mapp = hp.sphtfunc.alm2map(blm, nside, lmax=None)
    
    return mapp
    





def make_mapp_fft3d(vis_grid,nside):
    """
    OLD VERSION
    """

    img_cube1 = np.fft.fftshift(   np.fft.fftn(  np.fft.ifftshift(vis_grid)))    
    
    #img_cube1 =   np.fft.ifftn(  np.fft.ifftshift(vis_grid))  
    #img_cube1 = np.fft.ifftshift(   np.fft.ifftn(  vis_grid))  
    #img_cube1 = np.fft.fftn(  vis_grid)  
    
    
    img_cube1 = np.flip(img_cube1,axis=0)
    img_cube1 = np.flip(img_cube1,axis=1)
    img_cube1 = np.flip(img_cube1,axis=2)
    n_mesh = img_cube1.shape[0]
    
    img_cube = np.zeros((n_mesh+2,n_mesh+2,n_mesh+2),dtype=np.complex128)
    
    img_cube[1:-1,1:-1,1:-1] = img_cube1
   
    
    l = np.linspace(-1,1,n_mesh)
    m = np.linspace(-1,1,n_mesh)
    n = np.linspace(-1,1,n_mesh)
    
    ll,mm,nn = np.meshgrid(l,m,n)
    sph = np.array([ll.T,mm.T,nn.T]).T
    
    #print(sph.shape)
    
    npix = hp.nside2npix(nside)
    mapp = np.zeros(npix,dtype=np.complex128)
    
    #theta_ref = (90-35)/180*np.pi
    #phi_ref = 45/180*np.pi
    
    for i in range(npix):
        #print(i/npix*100,"%")
        xyz = hp.pix2vec(nside,i)
        dx = (xyz[0]-l)**2
        ix = np.argwhere(dx==np.min(dx))[0,0] +1
        dy = (xyz[1]-m)**2
        iy = np.argwhere(dy==np.min(dy))[0,0] +1
        dz = (xyz[2]-n)**2
        iz = np.argwhere(dz==np.min(dz))[0,0] +1
        
        val = 0
        val += img_cube[ix,iy,iz]#*5

        #val += np.sum(img_cube[ix-1:ix+2,iy-1:iy+2,iz-1:iz+2])
        #val /= (5+3*3*3)
        
        #val = 10*(img_cube[ix,iy,iz] + img_cube[ix+1,iy,iz] + img_cube[ix-1,iy,iz])
        #val += 5*img_cube[ix,iy+1,iz] + img_cube[ix+1,iy+1,iz] + img_cube[ix-1,iy+1,iz]
        #val += 5*img_cube[ix,iy-1,iz] + img_cube[ix+1,iy-1,iz] + img_cube[ix-1,iy-1,iz]
        #val += 5*img_cube[ix,iy,iz+1] + img_cube[ix+1,iy,iz+1] + img_cube[ix-1,iy,iz+1]
        #val += 5*img_cube[ix,iy,iz-1] + img_cube[ix+1,iy,iz-1] + img_cube[ix-1,iy,iz-1]
        #val += img_cube[ix,iy+1,iz+1] + img_cube[ix+1,iy+1,iz+1] + img_cube[ix-1,iy+1,iz+1]
        #val += img_cube[ix,iy+1,iz-1] + img_cube[ix+1,iy+1,iz-1] + img_cube[ix-1,iy+1,iz-1]
        #val += img_cube[ix,iy-1,iz+1] + img_cube[ix+1,iy-1,iz+1] + img_cube[ix-1,iy-1,iz+1]
        #val += img_cube[ix,iy-1,iz+1] + img_cube[ix+1,iy-1,iz+1] + img_cube[ix-1,iy-1,iz+1]
        #val /= (10*1 + 5*6 + 1*8)
        
        #val #/= np.sqrt(1-l[ix-1]**2-m[iy-1]**2)
        
        
        #theta,phi = hp.pix2ang(nside,i)
        
        #ang_dist = 2*np.arcsin(np.sqrt( np.sin((theta-theta_ref)/2)**2 + np.sin(theta)*np.sin(theta_ref)*np.sin((phi-phi_ref)/2)**2  ))
        
        #val *= np.exp(-1j*(np.pi+2*ang_dist))
        
        
        
        
        mapp[i] = val #np.abs(img_cube[ix,iy,iz])
    
    #mapp /= 4*np.pi**2*np.sqrt(n_mesh)  # comes out of nowhere 
    
    return mapp
    
    
    
    
    
   
def compute_fft(bl,vis,pointing_ang,fov,n_mesh):
    """
    UNUSED
    """
    mrot = R.from_euler('zy',np.array([pointing_ang[1],pointing_ang[0]]))
    bl_proj = mrot.apply(bl)
    w = bl_proj[:,2]
    vis *= np.exp(2j*np.pi*w)
    
    du = 1/(2*np.sin(fov/2))
    
    vis_grid = np.zeros((n_mesh,n_mesh),dtype=np.complex128)
    for i in range(bl_proj.shape[0]):
        iu = int(n_mesh/2 + bl_proj[i,0]/du)
        iv = int(n_mesh/2 + bl_proj[i,1]/du)
        if (np.abs(iu-n_mesh/2)<n_mesh/2) and (np.abs(iv-n_mesh/2)<n_mesh/2): 
            vis_grid[iu,iv] = vis[i]
    
    dirty = np.fft.fftshift(   np.fft.fftn(  np.fft.ifftshift(vis_grid)))
    
    l = np.linspace(-1/du/2,1/du/2,n_mesh)
    m = np.linspace(-1/du/2,1/du/2,n_mesh)
    ll,mm = np.meshgrid(l,m)
    corr = np.sqrt(1-ll**2-mm**2)
    
    dirty_corr = dirty.real / corr
    
    return dirty_corr
    
    
    
    

   
  
        
  
class Fit_gauss_fft(object):
    """
    UNUSED
    """
    def __init__(self,dirty, pointing_ang, fov):
        self.dirty = dirty
        self.pointing = pointing_ang
        self.fov = fov
        
        self.n_mesh = dirty.shape[0]
        
        self.min = np.min(self.dirty)
        self.factor = np.max(self.dirty-self.min)
        self.data = (self.dirty-self.min)/self.factor
        
        self.p0 = np.array([1, self.n_mesh/2, self.n_mesh/2, 3, 0])
        
        self.bnds = ([0, self.n_mesh/4, self.n_mesh/4, 1e-10,0],
                     [1.5, self.n_mesh*3/4, self.n_mesh*3/4, self.n_mesh/4,1])
        
    def run_fit(self):
        
        l = np.arange(self.n_mesh)
        m = np.arange(self.n_mesh)
        ll,mm = np.meshgrid(l,m)
        
        self.errorfunction = lambda p: np.ravel(self.data -  gaussian2d(*p)(ll,mm) )
        
        res = optimize.least_squares(self.errorfunction, self.p0, bounds = self.bnds)
        
        p = res.x
        
        err = np.sqrt(np.mean(self.errorfunction(p) **2))
        res.success = self.validate_fit(p,err,res.success)
        
        
        amp = (p[0]*self.factor)+self.min
        
        phi = self.pointing[1] - (p[1]-self.n_mesh/2)/self.n_mesh * self.fov
        theta = self.pointing[0] + (p[2]-self.n_mesh/2)/self.n_mesh * self.fov
        
        res = np.array([amp,theta,phi])
        
        return res
        
        
        
        
    def validate_fit(self,p,err,success):
        
        amp_bool = p[0] > 0.1
        ##############sig_bool = p[3] < self.radius/20#resol/2
        sig_bool = p[3] < self.n_mesh/10 #??????????????????????????
        err_bool = err < 0.1#0.01
        self.success_fit = bool(success) and amp_bool and sig_bool and err_bool
        
        print("SUCESS :",bool(success), amp_bool, sig_bool, err_bool,self.success_fit)
        #print(p[0],p[3],self.radius/20,err)
        
        return self.success_fit
        







            
            
class Sky_old(object):   

    def __init__(self, source_list=None):
        if source_list is not None:
            self.source_list = source_list
        else:
            self.source_list = []
        
        self._ns = self.ns
        self._source_pos = self.source_pos

        
    @property
    def ns(self):
        return len(self.source_list)
    
    @property
    def source_pos(self):
        source_pos = np.zeros((self._ns,3))
        for i in range(self._ns):
            source_pos[i,:] = self.source_list[i].Coord.vec
        self._source_pos = source_pos
        return source_pos
    
    @property
    def fluxes(self):
        fluxes = np.zeros((self._ns))
        for i in range(self._ns):
            fluxes[i] = self.source_list[i].flux.to_value(u.Jy)
        self._fluxes = fluxes
        return fluxes
    
    def __getitem__(self,n):
        return self.source_list[n]
    
    
    def update_source_list(self):
        self.source_list = []
        ns = self._source_pos.shape[0]
        for i in range(ns):
            Coord = Position(pos=self._source_pos[i])
            flux = self._fluxes[i]
            S = Source(Coord=Coord,flux=flux)
            self.add_source(S,no_update=True)
        self._ns=self.ns
        
        
    def add_source(self,Source,no_update=False):
        self.source_list.append(Source)
        if not no_update:
            self._ns=self.ns
            self._fluxes = self.fluxes
            self._source_pos = self.source_pos
    
    
    def apply_rotation(self,Rot):
        for i in range(self._ns):
            self.source_list[i].Coord.apply_rotation(Rot)
        self._source_pos = self.source_pos


    def apply_flip(self):
        self._source_pos = self._source_pos * np.array([1,1,-1])
        self.update_source_list()

    def copy(self):
        copy = Sky()
        cpoy._source_pos = np.copy(self.source_pos)
        copy._flux = np.copy(self.fluxes)
        copy.update_source_list()
        return copy

        
        
    def setup_source_p(self,ns,freq):
        sources, fluxes = get_point_sources(
            freq=freq,
            center=SkyCoord(ra=0*u.deg, dec=0*u.deg),
            radius=180*u.deg
        )    
        
        arg = np.argsort(-fluxes)[:ns]
        
        self._ns = ns
        self._fluxes = fluxes[arg]
        
        theta = (np.pi/2-sources.dec.rad)[arg]
        phi = (sources.ra.rad)[arg]
        ang = np.array([theta,phi]).T
        
        source_pos = sph2cart(ang)

        self._source_pos = source_pos
        
        self.update_source_list()
        
        
    def plot(self):
        mapp_plot = np.zeros(hp.nside2npix(2**4))
        hp.mollview(mapp_plot)
        hp.projscatter(hp.vec2ang(self.source_pos), marker='x', color='r')        
        


