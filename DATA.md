# RST_DATA

## Introduction

The data associated to this repository is available from Zenodo (Rouillé et al, 2023b), and accessible here: https://doi.org/10.5281/zenodo.7603149

The data are the output of various runs of the simulation pipeline. Simulations were run 
for various interferometer configurations, frequencies, noise levels and random draws.

## Data organization

The Zenodo record constains a ZIP archive file named `RST_DATA.zip``, which shall be downloaded and unzipped into the [Data/](Data/) directory of this code repository. 

The content of the ZIP archive file is organized as follows.

The folders named `Configuration_XX` contain results generated with a given configuration. The configurations are randomly generated following the process described in Sec. 3.1 of Rouillé et al (2023a).

They also contain the Radio Source Tracker (RST) simulation outputs named `Simu1_fFFF_noiseNNNNN_DD.json`. They are formated in Javascript Object Notation (JSON). `FFF` stands for the frequency $\nu_2$ in kHz. `NNNNN` stands for the noise level $\alpha$ as defined in the article (for instance if $\alpha=0.03$, `NNNNN`=00300). `DD` stands for the random draw number.

Ten configuration are provided. 
- Configuration #2-10 contain the results for 3 frequencies and for a noise of level of 0 with a single random draw and for a noise level of 0.03 with 10 random draws.
- Configuration #1 was used to study the influence of the frequency, the noise with multiple random draws. This folder contains outputs for 3 frequencies value (240, 400, 800). For each frequency, the noise level varies {0, 0.0001, 0.001, 0.003, 0.005, 0.007, 0.01, 0.03, 0.05, 0.07, 0.1}. For each level of noise the simulation was run 10 times with different random draws execpt for a noise level of 0 (where a single draw is performed).


## Data content

These file are saved as python dictionary. Their various keys are described below. Each file can be used to create an `Interferometer` object in order to setup the instrumental configuration for a simulation.

### Instrumental configuration variables

* `satpos` [array of dimension 2 ($n_\text{sat}$, 3)]: represents the position of the satellites in the system frame in meters. With $n_\text{sat}$ being the number of satellites.
* `baseline_mode` [string]: tells which baselines are considered. Four choice are possible: `solo_diag` only the autocorrelations, `tril_nodiag` every baselines, `tril` every baselines with the autocorrelations, `all` every beselines and their counterpart and the autocorrelations.
* `skyrot` [array of dimension 1 (4)]: represents the quaternion of the rotation of the transform that links the system frame and the absolute frame. The quaternion is shaped as $[x,y,z,w]$.
* `flip_bool` [boolean]: reprenting whether a reflection was added in the simulated transform. True if there is a reflection False otherwise.
* `freq_gsm` [integer or float]: represents the frequency in MHz of the reference sky used as input to model the ultra low frequency sky.
* `n_ps_input` [integer]: number of sources modeled as point sources in the model ultra low frequency sky.
* `vis` [array of dimension 3 (2,$n_\text{freq}$, $n_{bl}$)]:  represents the visibilities cross product for every baseline considered in Jy. The first axis separates the real and imaginary parts respectively. The second axis represents the various frequencies for which the visibilities were computed.
* `vis_sp`: same as for `vis` except that it only considers the point sources
* `vis_map`: same as for `vis` except that it only considers the continuum emission
* `gain_ps_continuum` [float]: rayleigh-jeans gain in calibration gain between the point sources amplitude model and the continuum emission amplitude model.
* `sysT` [array of dimension 1 ($n_{freq}$)]: represents the system temperaturùe in K for the frequencies considered.


### Run parameters

* `freq` [array of dimension 1 (2)]: frequencies $\nu_1$ and $\nu_2$ in kHz.
* `freq_id` [array of integer of dimension 1 (2)]: index of these frequencies in the frequency list of the object `Iterferometer` (python indexation starting at 0).
* `alpha` [float]: value of the parameter $\alpha$.
* `draw_number` [integer]: number of the random draw.
* `n_iteration` [integer]: number of iteration of the source detection loop.
* `mask_radius` [float]: radius in radian of the circular mask applied to flag out already explored regions.


### Intermediate variables

* `vis_w_noise` [array of dimension 2 (2, $n_{bl}$)]: visibilities in Jy with noise added according to `sysT` and $\alpha$.
* `found_sources_pos` [array of dimension 2 ($n_\text{s, mes}$,3)]: unitary vectors giving the direction of the sources found after the source detection loop. $n_\text{sources}$ is unknown and range between 0 and $n_\text{iteration}$.
* `found_sources_flux` [array of dimension 1 ($n_\text{s, mes}$)]: flux in Jy of the sources found after the source detection loop. 
* `sources_params` [array of dimension 2 ($n_\text{s,mes}$, 5)]: list the parameters that validates the fit of the sources. [0] is the a boolean telling whether the fitting algorithm did not crashed, [1] is the fitted amplitude of the source, [2] is the fitted angular radius of the source, [3] is the average absolute difference between pixel values and the fitted function and [4] is the boolean telling whether the fitted source is validated.
* `beam_size` [float]: diameter in radian of the maximum angular distance expected between the measured direction of a source and its true direction.
* `s_mes` [array of dimension 2 ($n_\text{s,mes}$, 3)]: unitary vectors representing the direction of the measured sources used to compute the pattern match.
* `s_cat` [array of dimension 2 ($n_\text{s,cat}$, 3)]: unitary vectors representing the direction of the catalog sources used to compute the pattern match.
* `permut_bool` [array of dimension 3 ($(n_\text{s,cat}-2)(n_\text{s,cat}-1)/2$ , $(n_\text{s,mes}-2)(n_\text{s,mes}-1)/2$ , 3 )]: boolean table representing the match between triangles of catalog sources and measured sources for each of the 3 criterion $(a,b,\gamma)$.
* `permut` [array of dimension 2 ($n_\text{s,cat}, n_\text{s,mes})$)]: table counting the number of vote received per pair of sources between the two lists. 




### Results

* `success` [boolean]: tells whether the run was successful. True if it was able to compute a transform, False otherwise.
* `q_fit` [array of dimension 1 (4)]: quaternion of the transform that links the system frame and the absolute frame (without any flip).
* `flip_fit` [boolean]: boolean reprenting whether a reflection was measured within transform. True if there is a reflection False otherwise.
* `accuracy` [float]: accuracy in acrmin of the returned transform. Maximum angular distance error that can bre introduce due to transform inaccuracy.
* `run_time` [float]: time in seconds of the computation time taken by the run

