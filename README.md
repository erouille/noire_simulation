# Radio Source Tracker for NOIRE

This project is meant to simulate the imaging capability of a space based interferometer.
It is equipped with a star-tracker function based on images produced by the interferomter.
This function is called Radio-Source Tracker (RST) and is detailled in its associated article (Rouillé et al. 2023a).
It also comes with a simulation of the radio sky at very low frequency (<2MHz).


## Dependencies

* Numpy
* Matplotlib
* Astropy
* Healpy
* Scipy
* Dask
* pySHtools
* pathlib
* Nenupy (this dependency will be removed in futur update)


## Architecture

* The package [interfero_classes.py](interfero_classes.py) contains the main functions and objects needed to run the jupyter notebooks.
* Intermediate results and outputs are stored in the [Data/](Data/) directory.
* The [DATA](DATA.md) file described how to load the previously computed data (Rouillé et al 2023b), as used for the RST paper (Rouillé et al 2023a)
* the [REFERENCE](REFERENCE.md) file contains the bibliographic references.  

## Notebook description

### Notebook 1: [Pre-process.ipynb](Pre-process.ipynb) 
This notebook pre-process the visibilities measured by the interferometer.
The interferometer is set up in this notebook.
The instrumental configuration is stored in an 'Interferometer' object, which is saved in as a numpy file named 'Simu1_interferometer.npy', created in the Data directory.


### Notebook 2: [Pipeline_example.ipynb](Pipeline_example.ipynb)
This notebook highlights how the main pipeline of the RST function works.
 

### Notebook 3: [Post-process_multi_run.ipynb](Post-process_multi_run.ipynb)
This notebook computes the full RST pipeline multiple times according the user's inputs.
This notebook has been used to generate the data at the origin of the plot displayed in the article (Rouille E et al. 2023a).
The output of these runs are stored in the 'Data' directory.


### Notebook 4: [Article_figures_notebook.ipynb](Article_figures_notebook.ipynb)
This notebook uses the output of Notebook 3 in order to generate the same plot as showed in the article.
The figures can be stored inside a directory 'Data/Figures/'.
